/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Controllers;

import com.mycompany.Entities.User1;
import com.codename1.components.InteractionDialog;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.animations.CommonTransitions;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.UIManager;
import com.mycompany.Forms.AccueilForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.Entities.Product;
import com.mycompany.Forms.LoginForm;
import com.mycompany.Forms.MainWindowForm;
import com.mycompany.Forms.PaymentForm;
import com.mycompany.myapp.PIDEV;

public class UserController {
   private Resources theme111;
    private ConnectionRequest connectionRequest;
    public static Form LOGIN;

    public void login(User1 user, Resources res) {
         
        connectionRequest = new ConnectionRequest() {
            List<User1> users = new ArrayList<>();
            User1 userkh = new User1();
            public String result;

            public String message;

            @Override
            protected void readResponse(InputStream in) throws IOException {
                    theme111 = UIManager.initFirstTheme("/theme1");
              

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");
                    Map<String, Object> data = json.parseJSON(reader);
                    System.out.println(data);
                    result = (String) data.get("result");
                    System.out.println(result);
                    if (result.equals("success")) {
                        Map<String, Object> userData = (Map<String, Object>) data.get("data");
                        users.clear();
                        int id = 0;
                        PIDEV.user = new User1();
                        PIDEV.mainForm = new MainWindowForm(PIDEV.theme);
                        PIDEV.mainForm.animateLayout(1000);
                        PIDEV.mainForm.show();
                        try {
                            Integer.parseInt((String) userData.get("id"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        users.add(User1.parseUser(userData));
                        System.out.println(users);

                    } else {
                        message = (String) data.get("message");
                        Dialog.show("Warning", "Login Failed.\n" + message, "OK", "Cancel");

                        System.out.println("okkkk");
                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                PIDEV.loginForm.hideProgress();

                for (User1 obj : users) {
//               
                    User1.setCurrentuser(obj);

                    if (result.equals("success")) {
                        PIDEV.user = user;
                         theme111 = UIManager.initFirstTheme("/theme1");
                        PIDEV.mainForm = new MainWindowForm(theme111);
                        PIDEV.mainForm.animateLayout(1000);
                        PIDEV.mainForm.show();
                    } else {

                        Dialog.show("Warning", "Login Failed.\n" + message, "OK", "Cancel");
                        PIDEV.loginForm.hideProgress();
                    }

                }
            }
            //   }
        };
        connectionRequest.setUrl("http://localhost/mobile/login.php?email=" + user.getEmail() + "&password=" + user.getPassword());
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    public void addUser(User1 user, Resources theme) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
                PIDEV.signupForm.hideProgress();
                InteractionDialog dlg = new InteractionDialog("Registration");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new Label("Registration successful "));
                Button close = new Button("Close");
                close.addActionListener((ActionListener) (ActionEvent evt) -> {
                    dlg.dispose();
                    LoginForm login = new LoginForm(theme);
                    login.getLOGIN().show();
                });
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(0, 0, 0, 0);

            }
        };
        String url = "http://localhost/mobile/insertuser.php?username="
                + user.getUsername() + "&email=" + user.getEmail() + "&password=" + user.getPassword()
                + "&nom=" + user.getNom() + "&prenom=" + user.getPrenom() + "&typeuser=" + user.getTypeuser()
                + "&solde=" + user.getSolde() + "&address=" + user.getAddress() + "&city=" + user.getCity() + "&zip=" + user.getZip() + "&country=" + user.getCountry();
        System.err.println(url);

        connectionRequest.setUrl(url);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void updateUser(User1 user) {
        connectionRequest = new ConnectionRequest() {
            List<User1> users = new ArrayList<>();
            User1 userkh = new User1();

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content = (List<Map<String, Object>>) data.get("root");
                    users.clear();
                    for (Map<String, Object> obj : content) {
                        System.out.println(obj);
                        System.out.println("-----------");
                        users.add(User1.parseUser(obj));
                        System.out.println(users);
                        System.out.println(obj.get("address"));
                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

            protected void postResponse() {
                for (User1 obj : users) {
                    User1.setCurrentuser(obj);
                }
                InteractionDialog dlg = new InteractionDialog("Update");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new Label("Update successful "));
                Button close = new Button("Close");
                close.addActionListener((ActionListener) (ActionEvent evt) -> {
                    dlg.dispose();
                    PIDEV.mainForm = new MainWindowForm(PIDEV.theme);
                    PIDEV.mainForm.show();
                    for (User1 obj : users) {
                        User1.setCurrentuser(obj);
                    }
                });
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(0, 0, 0, 0);

            }
        };
        String url = "http://localhost/mobile/update.php?username="
                + user.getUsername() + "&email=" + user.getEmail() + "&password=" + user.getPassword()
                + "&nom=" + user.getNom() + "&prenom=" + user.getPrenom() + "&address=" + user.getAddress() + "&city=" + user.getCity() + "&zip=" + user.getZip()
                + "&country=" + user.getCountry() + "&id=" + User1.getCurrentUser().id;
        System.err.println(url);
        System.out.println(User1.getCurrentUser().id);

        connectionRequest.setUrl(url);
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    public List<Product> fetchProducts(int nOffset) {
        ConnectionRequest connectionRequest;
        List<Product> products = new ArrayList<>();
        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("data");
                    products.clear();
                    for (Map<String, Object> obj : content) {
                        products.add(Product.parseProduct(obj));
                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
            }
        };
        connectionRequest.setUrl("http://localhost/mobile/product.php?offset=" + nOffset);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        return products;
    }

    public void order(String params) {
        ConnectionRequest connectionRequest;

        connectionRequest = new ConnectionRequest() {
            String result = "";
            String message = "";

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    System.out.println("datta !!! :::");
                    System.out.println(data);
                    result = (String) data.get("result");
                    System.out.println(result);
                    System.out.println(data.get("OrderCity"));
                    if (result.equals("failed")) {
                        System.out.println("message el try");
                        message = (String) data.get("message");
                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                ((PaymentForm) PIDEV.paymentForm).hideProgress();
                if (result.equals("success")) {
                    PIDEV.mainForm = new MainWindowForm(PIDEV.theme);
                    PIDEV.mainForm.show();
                } else {
                    Dialog.show("Warning", "Ordering Failed.\n" + message, "OK", "Cancel");
                    System.out.println("else mta3 el poste");
                }
            }

        };

        connectionRequest.setPost(true);
        connectionRequest.setHttpMethod("POST");
        connectionRequest.addArgument("data", params);
        connectionRequest.setUrl("http://localhost/mobile/order.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

}
