/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.codename1.googlemaps.MapContainer;
import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.maps.Coord;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
public class gmap {
    
      private Form current;
   public void init(Object context) {
        try {
            Resources theme = Resources.openLayered("/theme");
            UIManager.getInstance().setThemeProps(theme.getTheme(theme.getThemeResourceNames()[0]));
        } catch(IOException e){
            e.printStackTrace();
        }
    }
    
    public void  start(Form hi,int id, int nbpartcipant, int nbpartcipantMax, String nom, Image img, String Lieu, String descrr, String date, float lon, float lat , Resources res) {

        hi.setLayout(new BorderLayout());
    MapContainer cnt = new MapContainer();
           BrowserComponent browser = new BrowserComponent();
        Container map=new Container();
        browser.setScrollVisible(true);
           Command back = new Command("") {

            @Override
            public void actionPerformed(ActionEvent evt) {
                new EventsForm(res).show();
                
            }

        };  
        
        Image img1 = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, UIManager.getInstance().getComponentStyle("TitleCommand"));
        back.setIcon(img1);
       hi.getToolbar().addCommandToLeftBar(back);
       hi.getToolbar().setTitleCentered(true);
        hi.setBackCommand(back);
 //browser.setURL("https://www.google.tn/maps/place/"+e.getLieu()+"/");
 //map.add(browser);
        hi.addComponent(BorderLayout.CENTER, cnt);  try {int mm = Display.getInstance().convertToPixels(3);
        EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(mm * 3, mm * 4, 0), false);
                    cnt.setCameraPosition(new Coord(lon, lat));
                    cnt.addMarker(EncodedImage.create("/marker.png"), new Coord(lon,lat), "Hi marker", "Optional long description", new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            Dialog.show("Marker Clicked!", "You clicked the marker", "OK", null);
                        }
                    });
                } catch(IOException err) {
                    // since the image is iin the jar this is unlikely
                    err.printStackTrace();
                }
      

        
        hi.show();
        
    }

    public void stop() {
        current = Display.getInstance().getCurrent();
    }
    
    public void destroy() {
    }
}


