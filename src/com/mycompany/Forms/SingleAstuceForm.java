/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mycompany.Forms;

import com.mycompany.Entities.Session;
import com.mycompany.Entities.Astuce;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import org.json.JSONArray;
import org.json.JSONObject;


public class SingleAstuceForm extends BaseForm {
    private final Button removeBtn;
 private final Container mainContainer,mainC,mainCo;
    public SingleAstuceForm(Resources res) {
        super("", BoxLayout.y());
//        Toolbar tb = new Toolbar(true);
//        setToolbar(tb);
//        getTitleArea().setUIID("Container");
//        setTitle("");
//        getContentPane().setScrollVisible(false);
//        Form previous = Display.getInstance().getCurrent();
       
//          tb.setBackCommand("",e -> {
// new ClassifiedForm(res).show();
//       });
         getStyle().setBgColor(0xe3e3e3);
        Tabs swipe = new Tabs();
   removeBtn = new Button("Delete");
       
    mainContainer = new Container();
     mainC = new Container();
     mainCo = new Container();
  
        Label spacer1 = new Label();
        Label spacer2 = new Label();
         Command back = new Command("") {

            @Override
            public void actionPerformed(ActionEvent evt) {
                new AstuceForm(res).show();
                
            }

        };  
        
        Image img = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, UIManager.getInstance().getComponentStyle("TitleCommand"));
        back.setIcon(img);
       getToolbar().addCommandToLeftBar(back);
        getToolbar().setTitleCentered(true);
        setBackCommand(back);
         ConnectionRequest con= new ConnectionRequest();
         
             removeBtn.addActionListener(new ActionListener() {
                 
            public void actionPerformed(ActionEvent evt) {
                
                  con.setUrl("http://localhost/mobile/mobile2/connect.php");
        con.setUrl("http://localhost/mobile/mobile2/remove1.php?id_astuce="+Session.getSingleAdId());
                 Dialog.show("","Suppression effectuer avec succes", "ok",null);
                      new ClassifiedForm(res).show();                                                
              NetworkManager.getInstance().addToQueue(con);
            }
        }); 
      
        
        con.setUrl("http://localhost/mobile/mobile2/connect.php");
        con.setUrl("http://localhost/mobile/mobile2/searchAstuce1.php?id_astuce="+Session.getSingleAdId());
        System.out.println("before connection!");
        con.addResponseListener((NetworkEvent evt1) -> {
               
            String response = new String(con.getResponseData());
            
             if(!response.equals("error")){
                JSONArray a = new JSONArray(response);
                for(Object o : a){
                JSONObject tmp = new JSONObject(o.toString());
                    Image adPic = URLImage.createToStorage((EncodedImage) res.getImage("1461054823-6658-artwork.png"), 
                                  tmp.getString("image_astuce"), 
                                  ADS_URL + tmp.getString("image_astuce"));
                    System.out.println(ADS_URL + tmp.getString("image_astuce"));
                    setTitle(tmp.getString("title_astuce"));
//                    addTab(swipe, 
//                           res.getImage("tmp.png"), 
//                           spacer1, 
//                           "", 
//                           "0 Comments", 
//                           "",
//                           tmp.getInt("id_astuce"),
//                           res);
                    ScaleImageLabel image = new ScaleImageLabel(adPic);
                    TextArea desc = new TextArea(tmp.getString("description_astuce"));
                    desc.setEditable(false);
                    Container singleAd = LayeredLayout.encloseIn(
                                            BoxLayout.encloseY(
                                                    image,
                                                    BoxLayout.encloseX(
                                                            
                                                            new Label("Game name : "),
                                                            new Label(tmp.getString("title_astuce"))
                                                    ),
                                                    createLineSeparator(),
                                                    new Label("Description:"),
                                                    createLineSeparator(),
                                                    desc                                                   
                                                    
                                            )   
                            
                                        );
  
                    add(singleAd);
                    
             
                }
                
             
        addStringValue("",removeBtn);
     
       
            }else
                Dialog.show("Erreur","Probleme d\'affichage", "ok",null);
            //
        });
        NetworkManager.getInstance().addToQueue(con);
        
          

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();
       
        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);

        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        
        
        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));
        


    }
    
    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();
        
        
        
    }
    
    
    
    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text, int id_jeu,  Resources res) {
        
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
        
        ScaleImageLabel image = new ScaleImageLabel(img);
        image.setUIID("Container");
        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
      
        Container page1 = 
            LayeredLayout.encloseIn(
                image,
                overlay,
                BorderLayout.south(
                    BoxLayout.encloseY(
                            new SpanLabel(text, "LargeWhiteText"),
                            new SpanLabel("", "LargeWhiteText"),
                            spacer
                        )
                )
            );
        

        swipe.addTab("", page1);
        
    
 
    }
    
    
    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
