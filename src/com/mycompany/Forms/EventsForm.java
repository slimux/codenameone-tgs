/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteScrollAdapter;
import com.codename1.components.MultiButton;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.facebook.FaceBookAccess;
import com.codename1.facebook.User;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.L10NManager;
import com.codename1.l10n.ParseException;
import com.codename1.social.FacebookConnect;
import com.codename1.social.LoginCallback;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.NavigationCommand;
import com.codename1.ui.Slider;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.list.GenericListCellRenderer;
import com.codename1.ui.list.MultiList;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.Validator;
import com.mycompany.Controllers.UserController;
import com.mycompany.Entities.Product;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Session;
import static com.mycompany.Forms.MainWindowForm.orderList;
import com.mycompany.myapp.PIDEV;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Khaoula
 */
public class EventsForm extends BaseForm {
Container ctn = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        private Resources theme111;  
        Container ctn2 = BorderLayout.center(ctn);
    public EventsForm(Resources res) {
getStyle().setBgColor(0xe3e3e3);
        side();

        setTitle("Events");

        MultiButton orderMb = new MultiButton();

        ConnectionRequest con = new ConnectionRequest();

        con.setUrl("http://localhost/mobile/mobile1/select.php");
        System.out.println("before connection!");
        con.addResponseListener((NetworkEvent evt1) -> {
            System.out.println("after  connection!");
            String response = new String(con.getResponseData());

            if (!response.equals("error")) {
                JSONArray a = new JSONArray(response);
                for (Object o : a) {
                    JSONObject tmp = new JSONObject(o.toString());
                    Image adPic = URLImage.createToStorage((EncodedImage) res.getImage("Calendar-256.png"),
                            "imageevent" + tmp.getString("image"),
                            tmp.getString("image"));


                    addButton( tmp.getInt("id"),  tmp.getInt("nbpartcipant"),
                            tmp.getInt("nbpartcipantMax"), tmp.getString("nom"),adPic, 
                            tmp.getString("lieu"),
                             tmp.getString("descr"),
                            tmp.getString("date"),
                       /*  Float.parseFloat(tmp.getString("leng"))*/34.1009f,
                         /* Float.parseFloat(tmp.getString("lat"))*/118.3235793f,
                           res);
                }

            } else {
                Dialog.show("Erreur", "Probleme d\'affichage", "ok", null);
            }
            //
        });
        NetworkManager.getInstance().addToQueue(con);
        /*
        addButton(res.getImage("news-item-1.jpg"), "Morbi per tincidunt tellus sit of amet eros laoreet.", false, 26, 32);
        addButton(res.getImage("news-item-2.jpg"), "Fusce ornare cursus masspretium tortor integer placera.", true, 15, 21);
        addButton(res.getImage("news-item-3.jpg"), "Maecenas eu risus blanscelerisque massa non amcorpe.", false, 36, 15);
        addButton(res.getImage("news-item-4.jpg"), "Pellentesque non lorem diam. Proin at ex sollicia.", false, 11, 9);
         */
    }

    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();
    }

   

    private void addButton(int id, int nbpartcipant, int nbpartcipantMax, String nom, Image img, String Lieu, String desc, String date, float lon, float lat , Resources res) {
int height = Display.getInstance().convertToPixels(19f);
     int width = Display.getInstance().convertToPixels(15f);
    Button image = new Button(img.fill(width, height));     image.setUIID("Label");
//      Container cnt = BorderLayout.west(image);
  //   cnt.setLeadComponent(image);
//       TextArea ta = new TextArea(title);
//       Label des = new Label(desc);
//       ta.setUIID("NewsTopLine");
//       ta.setEditable(false);
//
//       Label likes = new Label(likeCount + " Likes  ", "NewsBottomLine");
//       likes.setTextPosition(RIGHT);
//       if(!liked) {
//           FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
//       } else {
//           Style s = new Style(likes.getUnselectedStyle());
//           s.setFgColor(0xff2d55);
//           FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
//           likes.setIcon(heartImage);
//       }
//       Label comments = new Label(commentCount + " Comments", "NewsBottomLine");
//       FontImage.setMaterialIcon(likes, FontImage.MATERIAL_CHAT);
//       
//       
//       cnt.add(BorderLayout.CENTER, 
//               BoxLayout.encloseY(
//                       ta,
//                       des,
//                       BoxLayout.encloseX(likes, comments)
//               ));

 
               
  Label l=new Label(" ");
 MultiButton fourLinesIcon = new MultiButton(nom);
Container tl = TableLayout.encloseIn(1,l, fourLinesIcon);
        
        ctn2.setLeadComponent(fourLinesIcon);
fourLinesIcon.setIcon(img.fill(width, height));
fourLinesIcon.setTextLine2(Lieu);



 add(tl);
 fourLinesIcon.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent evt) {
        new EVentForm(id,nbpartcipant, nbpartcipantMax,nom,img,Lieu,desc,date, lon,lat ,res).show();

    }       
        















});

    }

    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if (b.isSelected()) {
                updateArrowPosition(b, arrow);
            }
        });
    }

  public void side() {
        
        Image m = PIDEV.theme.getImage("menu.png");
        Command profile = new Command("My Profile") {
            @Override
            public void actionPerformed(ActionEvent evt) {
                PIDEV.ProfileForm = new ProfileForm(PIDEV.theme);
                ((ProfileForm) PIDEV.ProfileForm).execute();
            }
        };
        getToolbar().addCommandToSideMenu(profile);
        Command c = new Command("FAVORITES");
        Label l = new Label("FAVORITES") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l.getStyle().setFgColor(255);
        l.setUIID("Separator");
        c.putClientProperty("SideComponent", l);
        this.addCommand(c);
        this.addCommand(new Command("Store", PIDEV.theme.getImage("cart.png")) {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new MainWindowForm(theme111).show();
             
            }

        });      this.addCommand(new Command("Games") {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new ClassifiedForm(theme111).show();
            //PIDEV.mainForm.show();
            }

        });
           this.addCommand(new Command("Events") {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
                 new EventsForm(theme111).show();
           
                
              
                
               
             
            }

        });
        this.addCommand(new Command("Video") {
            public void actionPerformed(ActionEvent evt) {
              theme111 = UIManager.initFirstTheme("/theme1");
                 new VidiosForme(theme111).show();
            }

        });
        
        Command c1 = new Command("ACTIONS");
        Label l1 = new Label("ACTIONS") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l1.setUIID("Separator");
        c1.putClientProperty("SideComponent", l1);
        this.addCommand(c1);
        this.addCommand(new Command("Exit") {

            public void actionPerformed(ActionEvent evt) {
                Display.getInstance().exitApplication();
            }
        });

        this.addCommand(new Command("Logout") {

            public void actionPerformed(ActionEvent evt) {
//                if(FacebookConnect.getInstance().isFacebookSDKSupported()) {
//                    FacebookConnect.getInstance().logout();
//                } else {
//                    FaceBookAccess.getInstance().logOut();
//                    Login.login(main);
//                }

                PIDEV.loginForm = new LoginForm(PIDEV.theme);
                PIDEV.loginForm.show();
            }
        });

    }
}
