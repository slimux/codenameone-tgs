/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.Controllers.UserController;
import com.mycompany.Entities.User1;

/**
 *
 * @author haythem
 */
public class SignUpForm extends GeneralForm{
    
    Form SIGNUP;
    TextField UN,FN,LN,EM,Address,City,Postal,Country,PWD1,PWD2;
    Button SignUp,Sign_In;
    
     public SignUpForm(Resources theme){ 
         super(theme);
        UIBuilder ui = new UIBuilder();
        SIGNUP = ui.createContainer(theme, "SIGNUP").getComponentForm();
        UN = (TextField) ui.findByName("UN", SIGNUP);
        UN.setConstraint(TextField.EMAILADDR);
        FN = (TextField) ui.findByName("FN", SIGNUP);
        FN.setConstraint(TextField.EMAILADDR);
        LN  = (TextField) ui.findByName("LN", SIGNUP);
        LN.setConstraint(TextField.EMAILADDR);
        EM  = (TextField) ui.findByName("EM", SIGNUP);
        EM.setConstraint(TextField.EMAILADDR);
        Address  = (TextField) ui.findByName("Address", SIGNUP);
        Address.setConstraint(TextField.EMAILADDR);
        City  = (TextField) ui.findByName("City", SIGNUP);
        City.setConstraint(TextField.EMAILADDR);
        Postal  = (TextField) ui.findByName("Postal", SIGNUP);
        Postal.setConstraint(TextField.EMAILADDR);
        Country  = (TextField) ui.findByName("Country", SIGNUP);
        Country.setConstraint(TextField.EMAILADDR);
        
        PWD1  = (TextField) ui.findByName("PWD1", SIGNUP);
        PWD1.setConstraint(com.codename1.ui.TextArea.PASSWORD);
        PWD2  = (TextField) ui.findByName("PWD2", SIGNUP);
        PWD2.setConstraint(com.codename1.ui.TextArea.PASSWORD);
        SignUp = (Button) ui.findByName("SignUp", SIGNUP);
        
        SignUp.addActionListener((ActionListener) (ActionEvent evt) -> {
            String usernameVal = UN.getText();
            String firstNameVal = FN.getText();
            String lastNameVal = LN.getText();
            String emailVal = EM.getText();
            String addressVal = Address.getText();
            String cityVal = City.getText();
            String postalVal = Postal.getText();
            String countryVal = Country.getText();
            String passwordVal = PWD1.getText();
            String passwordConfirmVal = PWD2.getText();
        User1 typedUser;
            if ("".equals(usernameVal) || "".equals(firstNameVal) || "".equals(lastNameVal) || "".equals(emailVal) 
                    || "".equals(passwordVal) || "".equals(passwordConfirmVal) || "".equals(addressVal) 
                    || "".equals(cityVal) || "".equals(postalVal) || "".equals(countryVal) ) {
                 Dialog.show("Warning", "Please complete registration form !! ", "OK", "Cancel");
            } else {
            
        if( !passwordVal.equals(passwordConfirmVal) )
            {
                 Dialog.show("Warning", "Password confirm is wrong. Please input correct matching password in here.", "OK", "Cancel");
            }
         else
            {
                 showProgress();
        typedUser = new User1(usernameVal, emailVal, passwordVal, firstNameVal, lastNameVal,"", 0, 0, addressVal,cityVal, postalVal, countryVal);            
        new  UserController().addUser(typedUser,theme);
            }
        }
        
        });   
        Sign_In = (Button) ui.findByName("Sign_In", SIGNUP);
        Sign_In.addActionListener((ActionListener) (ActionEvent evt) -> {
            LoginForm login = new LoginForm(theme);
            login.getLOGIN().animateLayout(1000);
            login.getLOGIN().show();
        }); 
        
    }

    public Form getSIGNUP() {
        return SIGNUP;
    }

    public void setSIGNUP(Form SIGNUP) {
        this.SIGNUP = SIGNUP;
    }

    public TextField getFN() {
        return FN;
    }

    public void setFN(TextField FN) {
        this.FN = FN;
    }

    public TextField getLN() {
        return LN;
    }

    public void setLN(TextField LN) {
        this.LN = LN;
    }

    public TextField getEM() {
        return EM;
    }

    public void setEM(TextField EM) {
        this.EM = EM;
    }

    public TextField getPWD1() {
        return PWD1;
    }

    public void setPWD1(TextField PWD1) {
        this.PWD1 = PWD1;
    }

    public TextField getPWD2() {
        return PWD2;
    }

    public void setPWD2(TextField PWD2) {
        this.PWD2 = PWD2;
    }

    public Button getSignUp() {
        return SignUp;
    }

    public void setSignUp(Button SignUp) {
        this.SignUp = SignUp;
    }
     
     
     public void show(){     
      SIGNUP.show(); 
    }
}
