/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.components.InfiniteProgress;
import com.codename1.components.ScaleImageLabel;
import com.codename1.facebook.FaceBookAccess;
import com.codename1.facebook.User;
import com.codename1.io.NetworkEvent;
import com.codename1.social.FacebookConnect;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.ComponentGroup;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.mycompany.Controllers.UserController;
import com.mycompany.Entities.User1;
import com.mycompany.myapp.PIDEV;
import java.io.IOException;

/**
 * GUI builder created Form
 *
 * @author haythem
 */
public class ProfileForm extends com.codename1.ui.Form {
    
     InfiniteProgress progress;
    Dialog progressDialog;
    public void showProgress()
    {      
        if( progressDialog == null)
        {
            progress = new InfiniteProgress();
            progressDialog = progress.showInifiniteBlocking();
        }
    }
    
    public void hideProgress()
    {
        if(progressDialog != null)
        {
            progressDialog.dispose();
            progressDialog = null;
        }
    }   

    public ProfileForm() {
        this(com.codename1.ui.util.Resources.getGlobalResources());
    }
    
    public ProfileForm(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
    }

//-- DON'T EDIT BELOW THIS LINE!!!


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
        setTitle("My Profile");
        setName("ProfileForm");
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
    
    @Override
    public void show() {
        super.show(); 
    }
    
     public void execute(){  
         
        getStyle().setBgColor(0xe3e3e3);        
        getToolbar().addSearchCommand(e -> {});
        Image img = PIDEV.theme.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 3) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 3);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
               
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(1,
                            FlowLayout.encloseCenter(
                                new Label(PIDEV.theme.getImage("profile.jpg"), "PictureWhiteBackgrond"))
                    )
                )
        ));
        
         TextFieldEx userName = new TextFieldEx("", "Username", 30, TextField.EMAILADDR);
         userName.setText(User1.getCurrentUser().username);
         TextFieldEx firstName = new TextFieldEx("", "FirstName", 30, TextField.EMAILADDR);
         firstName.setText(User1.getCurrentUser().prenom);
         TextFieldEx lastName = new TextFieldEx("", "LastName", 30, TextField.EMAILADDR);
         lastName.setText(User1.getCurrentUser().nom);
         TextFieldEx email = new TextFieldEx("", "E-mail", 30, TextField.EMAILADDR);
         email.setText(User1.getCurrentUser().email);
         TextFieldEx password = new TextFieldEx("", "Pasword", 30, TextField.PASSWORD);
         password.setText(User1.getCurrentUser().password);
        TextFieldEx orderShipAddressField = new TextFieldEx("", "Order Ship Address", 20, TextField.EMAILADDR) ;
        orderShipAddressField.setText(User1.getCurrentUser().address);
        TextFieldEx orderCityField = new TextFieldEx("", "Order City", 20, TextField.EMAILADDR) ;
        orderCityField.setText(User1.getCurrentUser().city);
        TextFieldEx orderZipField = new TextFieldEx("", "Order Zip Code", 20, TextField.NUMERIC) ;
        orderZipField.setText(User1.getCurrentUser().zip);
        TextFieldEx orderCountryField = new TextFieldEx("", "Order Country", 20, TextField.EMAILADDR) ;
        orderCountryField.setText(User1.getCurrentUser().country);
        Label o = new Label(" ") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        o.getStyle().setFgColor(255);
        o.setUIID("Separator");  
        
       
        Button saveButton = new Button("Update my account");
        saveButton.addActionListener(e -> {

       User1 updateUser = new User1(userName.getText(),email.getText(),password.getText(),lastName.getText(),firstName.getText(),orderShipAddressField.getText(),orderCityField.getText(),orderZipField.getText(),orderCountryField.getText());
        new UserController().updateUser(updateUser);
        
            System.out.println(User1.getCurrentUser().id);
        
        });
        
        
        
       Image m = PIDEV.theme.getImage("menu.png"); 
        Command profile=new Command("My Profile"){
            @Override
            public void actionPerformed(ActionEvent evt){ 
                PIDEV.ProfileForm = new ProfileForm(PIDEV.theme);
                ((ProfileForm)PIDEV.ProfileForm).execute();
            }
        };
        getToolbar().addCommandToSideMenu(profile);
        Command c = new Command("FAVORITES");
        Label l = new Label("FAVORITES") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l.getStyle().setFgColor(255);
        l.setUIID("Separator");        
        c.putClientProperty("SideComponent", l);
        this.addCommand(c);
        this.addCommand(new Command("Store", PIDEV.theme.getImage("cart.png")){
            public void actionPerformed(ActionEvent evt) {
                PIDEV.mainForm = new MainWindowForm(PIDEV.theme);
                    PIDEV.mainForm.show();
            }
            
        });
       Command c1 = new Command("ACTIONS");
        Label l1 = new Label("ACTIONS") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l1.setUIID("Separator");
        c1.putClientProperty("SideComponent", l1);
        this.addCommand(c1);
        this.addCommand(new Command("Exit") {

            public void actionPerformed(ActionEvent evt) {
                Display.getInstance().exitApplication();
            }
        });
        
        this.addCommand(new Command("Logout") {

            public void actionPerformed(ActionEvent evt) {
                if(FacebookConnect.getInstance().isFacebookSDKSupported()) {
                    FacebookConnect.getInstance().logout();
                } else {
                    FaceBookAccess.getInstance().logOut();
                    PIDEV.loginForm = new LoginForm(PIDEV.theme);
                    PIDEV.loginForm.show();
                }

            
            }
        });
        
        Label spaceLabel;
        if(!Display.getInstance().isTablet() && Display.getInstance().getDeviceDensity() < Display.DENSITY_VERY_HIGH) {
            spaceLabel = new Label();
        } else {
            spaceLabel = new Label(" ");
        }
        
        Container by = BoxLayout.encloseY(spaceLabel,userName,firstName,lastName,
                email,password,orderShipAddressField,orderCityField,orderZipField,orderCountryField,o,saveButton);
        add(by);
        show();
        
     }
     
    
}
