/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.ui.TextField;

/**
 *
 * @author haythem
 */
public class TextFieldEx extends TextField{

    TextFieldEx(String text, String hint, int columns, int constraint) {
        super(text, hint, columns, constraint);
        getStyle().setFgColor(0x000000);
        getStyle().setBgColor(0xffffff);
    }
    
}
