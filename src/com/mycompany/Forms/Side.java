/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.ui.Command;
import com.codename1.ui.Display;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.PIDEV;

/**
 *
 * @author Khaoula
 */
public class Side  extends com.codename1.ui.Form{ 
        private Resources theme111;
   public Side() {
        
    }
    
    
        public void side() {
        
        Image m = PIDEV.theme.getImage("menu.png");
        Command profile = new Command("My Profile") {
            @Override
            public void actionPerformed(ActionEvent evt) {
                PIDEV.ProfileForm = new ProfileForm(PIDEV.theme);
                ((ProfileForm) PIDEV.ProfileForm).execute();
            }
        };
        getToolbar().addCommandToSideMenu(profile);
        Command c = new Command("FAVORITES");
        Label l = new Label("FAVORITES") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l.getStyle().setFgColor(255);
        l.setUIID("Separator");
        c.putClientProperty("SideComponent", l);
        this.addCommand(c);
        this.addCommand(new Command("Store", PIDEV.theme.getImage("cart.png")) {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new MainWindowForm(theme111).show();
             
            }

        });      this.addCommand(new Command("Games", PIDEV.theme.getImage("cart.png")) {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new ClassifiedForm(theme111).show();
            //PIDEV.mainForm.show();
            }

        });
           this.addCommand(new Command("Events", PIDEV.theme.getImage("cart.png")) {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
                 new EventsForm(theme111).show();
           
                
              
                
               
             
            }

        });
        this.addCommand(new Command("Video", PIDEV.theme.getImage("cart.png")) {
            public void actionPerformed(ActionEvent evt) {
                 theme111 = UIManager.initFirstTheme("/theme1");
                 new EventsForm(theme111).show();
            }

        });
        
        Command c1 = new Command("ACTIONS");
        Label l1 = new Label("ACTIONS") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l1.setUIID("Separator");
        c1.putClientProperty("SideComponent", l1);
        this.addCommand(c1);
        this.addCommand(new Command("Exit") {

            public void actionPerformed(ActionEvent evt) {
                Display.getInstance().exitApplication();
            }
        });

        this.addCommand(new Command("Logout") {

            public void actionPerformed(ActionEvent evt) {
//                if(FacebookConnect.getInstance().isFacebookSDKSupported()) {
//                    FacebookConnect.getInstance().logout();
//                } else {
//                    FaceBookAccess.getInstance().logOut();
//                    Login.login(main);
//                }

                PIDEV.loginForm = new LoginForm(PIDEV.theme);
                PIDEV.loginForm.show();
            }
        });

    }

}
