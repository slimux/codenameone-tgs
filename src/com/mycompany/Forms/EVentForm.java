/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.components.ImageViewer;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Session;
import org.json.JSONArray;
import org.json.JSONObject;
import com.mycompany.Entities.Commentaire;
import com.mycompany.myapp.PIDEV;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 *
 * @author Khaoula
 */
public class EVentForm extends BaseForm {
    SpanLabel descff;
     static int SR;Evenement e;TextArea text ;
    private final Button removeBtn,editBtn,astuceBtn, commenter;
   private final Container mainContainer,mainC,mainCo,comentaires;
    public EVentForm(int id, int nbpartcipant, int nbpartcipantMax, String nom, Image img, String Lieu, String descrr, String date, float lon, float lat , Resources res) throws IOException {
           super("", BoxLayout.y());e=new Evenement(id, nbpartcipant, nbpartcipantMax, nom, date, Lieu, descrr, date, lon, lat);
//        Toolbar tb = new Toolbar(true);
//        setToolbar(tb);
//        getTitleArea().setUIID("Container");
//        setTitle("");
//        getContentPane().setScrollVisible(false);
//        Form previous = Display.getInstance().getCurrent();
descff= new SpanLabel();
     getStyle().setBgColor(0xe3e3e3);
     comentaires= new Container(BoxLayout.y()) ; 
//       
//          tb.setBackCommand("",e -> {
// new ClassifiedForm(res).show();
//       });

setTitle(nom);
        Label spacer1 = new Label();
        Label spacer2 = new Label();
        text =new TextArea();
        Tabs swipe = new Tabs();
        commenter = new Button("Comment");
   removeBtn = new Button("Participate");
         astuceBtn = new Button("Afficher Astuce");
           editBtn = new Button("Modifier");
    mainContainer = new Container();
     mainC = new Container();
     mainCo = new Container();
     Command back = new Command("") {

            @Override
            public void actionPerformed(ActionEvent evt) {
                new EventsForm(res).show();
                
            }

        };  
        
        Image img1 = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, UIManager.getInstance().getComponentStyle("TitleCommand"));
        back.setIcon(img1);
       getToolbar().addCommandToLeftBar(back);
        getToolbar().setTitleCentered(true);
        setBackCommand(back);
         Form f=new Form(Lieu);
        Label l =new Label(nbpartcipant+"/"+nbpartcipantMax);
            
                 Label tlieu=new Label(Lieu);  
                 tlieu.addPointerPressedListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
              gmap g= new  gmap();
                g.start(f, id, nbpartcipant, nbpartcipantMax, nom, img,  Lieu,  descrr,  date,  lon,  lat ,res);
            }});
                
                
//                    Image adPic = URLImage.createToStorage((EncodedImage) res.getImage("jaquette-grand-theft-auto-v-playstation-4-ps4-cover-avant-g-1415122088.jpg"), 
//                                  tmp.getString("image"), 
//                                  ADS_URL + tmp.getString("image"));
//                    System.out.println(ADS_URL + tmp.getString("image"));
                   
                    ScaleImageLabel image = new ScaleImageLabel(img);
                    TextArea desc = new TextArea(descrr);
                    desc.setEditable(false);
                    Container singleAd = LayeredLayout.encloseIn(
                                            BoxLayout.encloseY(
                                                    image,
                                                    BoxLayout.encloseX(
                                                            
                                                            new Label("Title : "),
                                                            new Label(nom)
                                                    ),BoxLayout.encloseX(
                                                            
                                                            new Label("Particopation : "),
                                                           l
                                                    )
                                                    ,BoxLayout.encloseX(
                                                            
                                                        new ImageViewer(EncodedImage.create("/marker.png")),
                                                            tlieu
                                                    ),
                                                    BoxLayout.encloseX(
                                                            
                                                            new Label("Date : "),
                                                            new Label(date)
                                                    ),
                                                    createLineSeparator(),
                                                    new Label("Description:"),
                                                    createLineSeparator(),
                                                    desc                                               
                                                    
                                            )   
                            
                                        );
  
                    add(singleAd);
                    
             
         addStringValue("",removeBtn);        
        add(comentaires);        
        ListCommentaire(e);    
       
        
    addStringValue("",text);
         addStringValue("",commenter);
        commenter.addActionListener(es -> {
               CommenterAction(e);
       });
           removeBtn.addActionListener(es -> {
               l.setText(nbpartcipant+1+"/"+nbpartcipantMax);
            participerAction(e);
       });
           
          

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();
       
        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);

        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        
        
        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));
        
        

    
    }
    
    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();
        
        
        
    }
    
    
    
    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text, int id_jeu,  Resources res) {
        
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
        
        ScaleImageLabel image = new ScaleImageLabel(img);
        image.setUIID("Container");
        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
      
        Container page1 = 
            LayeredLayout.encloseIn(
                image,
                overlay,
                BorderLayout.south(
                    BoxLayout.encloseY(
                            new SpanLabel(text, "LargeWhiteText"),
                            new SpanLabel("", "LargeWhiteText"),
                            spacer
                        )
                )
            );
        

        swipe.addTab("", page1);
        
        
         
          
    }
    
    public void participerAction(Evenement e) {
        ConnectionRequest req = new ConnectionRequest();
        req.setUrl("http://localhost/mobile/mobile1/insert.php?idevenement=" +e.getId() + "&idmembre=" + "khaoula oueslati");

        req.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);

                if (s.equals("success")) {
                    updateEvent(e);
        // nbparticipant.setText("Participation :" + e.getNbpartcipant() + "/" + e.getNbpartcipantMax());
                    Dialog.show("Done", "you just partcipate  in"+e.getNom(), "Ok", null);

                }

            }
        });
        NetworkManager.getInstance().addToQueue(req);

    }
   public void updateEvent(Evenement e) {
        e.setNbpartcipant(e.getNbpartcipant() + 1);
        ConnectionRequest req2 = new ConnectionRequest();
        req2.setUrl("http://localhost/mobile/mobile1/updateevenement.php?idevenement=" + e.getId() + "&nb=" + e.getNbpartcipant());
        NetworkManager.getInstance().addToQueue(req2);

    }
    
    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
public void CommenterAction(Evenement e) {
        ConnectionRequest req4 = new ConnectionRequest();
     
        Commentaire cm=new Commentaire(PIDEV.user.nom, text.getText(), e.getId());
        req4.setUrl("http://localhost/mobile/mobile1/insertcommentaire.php?idevent=" + cm.getIdevent() + "&username=" + cm.getUsername() + "&date=" + cm.getDate() + "&contenu=" + cm.getContenu());

        req4.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);

                if (s.equals("success")) {
                    Dialog.show("Confirmation", "add ok", "Ok", null);
                    addCommentaire(cm);

                }
          
            }
        });
        NetworkManager.getInstance().addToQueue(req4);
    }
private void ListCommentaire(Evenement e) {
        ConnectionRequest req3 = new ConnectionRequest();
        req3.setUrl("http://localhost/mobile/mobile1/selectcommentaire.php?idevent=" + e.getId());
        ArrayList<Map<String, Object>> data2 = new ArrayList<>();        req3.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
ArrayList<Commentaire> es =getListCommentaire(new String(req3.getResponseData()));
                for (Commentaire cm : es) {
                    addCommentaire(cm);
                  //addStringValue("",comentaires); 
                }

es.clear();
                DefaultListModel<Map<String, Object>> model1 = new DefaultListModel<>(data2);

                //ml2.setModel(model1);
            }
        });
        NetworkManager.getInstance().addToQueue(req3);

    }
   public ArrayList<Commentaire> getListCommentaire(String json) {
        ArrayList<Commentaire> listEtudiants = new ArrayList<>();

        try {

            JSONParser j = new JSONParser();

            Map<String, Object> etudiants = j.parseJSON(new CharArrayReader(json.toCharArray()));

            System.out.println();
            List<Map<String, Object>> list = (List<Map<String, Object>>) etudiants.get("commentaire");

            for (Map<String, Object> obj : list) {
                Commentaire e = new Commentaire();
                e.setId(Integer.parseInt(obj.get("id").toString()));
                e.setIdevent(Integer.parseInt(obj.get("idevent").toString()));
                e.setDate(obj.get("date").toString());
                e.setContenu(obj.get("contenu").toString());
                e.setUsername(obj.get("username").toString());

                listEtudiants.add(e);

            }

        } catch (IOException ex) {
        }
        return listEtudiants;

    }
     public void addCommentaire(Commentaire cm) {
        Label l1 = new Label(cm.getUsername());
        Label l2 = new Label(cm.getDate());
       
 
        l1.setUIID("MultiLine1");
       l2.setUIID("MultiLine3");
       
     
       descff = new SpanLabel(cm.getContenu());
                    
       comentaires.add(l1);
             comentaires.add(l2);
       comentaires.add(descff);
        comentaires.add(createLineSeparator(0xeeeeee));
      
    }
}