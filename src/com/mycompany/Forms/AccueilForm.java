/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.facebook.FaceBookAccess;
import com.codename1.social.FacebookConnect;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import static com.mycompany.myapp.PIDEV.theme;

/**
 *
 * @author haythem
 */
public class AccueilForm extends GeneralForm{
    
    Form ACCUEIL;
    Label logo;
    Button Facebook;
    Button SU_R;
    Button SI_L;
   
    
    public AccueilForm(Resources theme){ 
       super(theme);
        UIBuilder ui = new UIBuilder();
       ACCUEIL = ui.createContainer(theme, "ACCUEIL").getComponentForm();
       SU_R = (Button) ui.findByName("SU_R", ACCUEIL);
       SU_R.addActionListener((ActionListener) (ActionEvent evt) -> {
         SignUpForm signup = new SignUpForm(theme);
               signup.getSIGNUP().animateLayout(1000);
               signup.getSIGNUP().show();  
       });  
       SI_L = (Button) ui.findByName("SI_L", ACCUEIL);
       SI_L.addActionListener((ActionListener) (ActionEvent evt) -> {
       LoginForm login = new LoginForm(theme);
                login.getLOGIN().animateLayout(20000);
               login.getLOGIN(). show();    
       });  
       
//         Facebook.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//                new FacebookMain().start();
//            }
//        });
       
    }

    public Form getACCUEIL() {
        return ACCUEIL;
    }

    public void setACCUEIL(Form ACCUEIL) {
        this.ACCUEIL = ACCUEIL;
    }

    public Button getFacebook() {
        return Facebook;
    }

    public void setFacebook(Button Facebook) {
        this.Facebook = Facebook;
    }

    public Button getSU_R() {
        return SU_R;
    }

    public void setSU_R(Button SU_R) {
        this.SU_R = SU_R;
    }

    public Button getSI_L() {
        return SI_L;
    }

    public void setSI_L(Button SI_L) {
        this.SI_L = SI_L;
    }
       
       
       public void show(){     
      ACCUEIL.show(); 
    }
       
}
