/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.components.WebBrowser;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.javascript.JSObject;
import com.codename1.javascript.JavascriptContext;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Session;

/**
 *
 * @author Khaoula
 */
public class VideoForm extends BaseForm {
     static int SR;Evenement e;
    private final Button removeBtn,editBtn,astuceBtn;
   private final Container mainContainer,mainC,mainCo;
   private BrowserComponent browser,brow;
    private JavascriptContext c,v;
    private JSObject map,live;
    public VideoForm( String titre,  String type_video, String date_ajout, String code_video,  String description ,Resources res) {
           super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("");
        getContentPane().setScrollVisible(false);
        Form previous = Display.getInstance().getCurrent();
     
         getStyle().setBgColor(0xe3e3e3);
       
          tb.setBackCommand("",e -> {
 new ClassifiedForm(res).show();
       });
        
        Tabs swipe = new Tabs();
   removeBtn = new Button("Participate");
         astuceBtn = new Button("Afficher Astuce");
           editBtn = new Button("Modifier");
    mainContainer = new Container();
     mainC = new Container();
     mainCo = new Container();
      
        Label spacer1 = new Label();
        Label spacer2 = new Label();
         
        
          WebBrowser b = new WebBrowser(){

            @Override
            public void onLoad(String url) {
                System.out.println("Loaded "+url);
                map = (JSObject)c.get("map");

            } 
            
        };
        browser = (BrowserComponent)b.getInternal();
        c = new JavascriptContext(browser);
        b.setURL("jar:///you.html");   
            
                
//                    Image adPic = URLImage.createToStorage((EncodedImage) res.getImage("jaquette-grand-theft-auto-v-playstation-4-ps4-cover-avant-g-1415122088.jpg"), 
//                                  tmp.getString("image"), 
//                                  ADS_URL + tmp.getString("image"));
//                    System.out.println(ADS_URL + tmp.getString("image"));
//                    addTab(swipe, 
//                           res.getImage("tmp.png"), 
//                           spacer1, 
//                           "", 
//                           "0 Comments", 
//                           "",
//                           1,
//                           res);
                  
                    TextArea desc = new TextArea(description);
                    desc.setEditable(false);
                    Container singleAd = LayeredLayout.encloseIn(
                                            BoxLayout.encloseY(
                                                  
                                                    BoxLayout.encloseX(
                                                            
                                                            new Label("Title : "),
                                                            new Label(titre)
                                                    )
                                                    , createLineSeparator(),
                                                    new Label("Description:"),
                                                    createLineSeparator(),
                                                    desc                ,
                                                    BoxLayout.encloseX(
                                                            
                                                            new Label("Date : "),
                                                            new Label(date_ajout)
                                                    ),b
                                                                                  
                                                    
                                            )   
                            
                                        );
  
                    add(singleAd);
                    
             
                
                
             
        addStringValue("",removeBtn);
     
       
       
        
          

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();
       
        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);

        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        
        
        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));
        
        


    }
    
    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();
        
        
        
    }
    
    
    
    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text, int id_jeu,  Resources res) {
        
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
        
        ScaleImageLabel image = new ScaleImageLabel(img);
        image.setUIID("Container");
        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
      
        Container page1 = 
            LayeredLayout.encloseIn(
                image,
                overlay,
                BorderLayout.south(
                    BoxLayout.encloseY(
                            new SpanLabel(text, "LargeWhiteText"),
                            new SpanLabel("", "LargeWhiteText"),
                            spacer
                        )
                )
            );
        

        swipe.addTab("", page1);
        
           removeBtn.addActionListener(es -> {
               
            participerAction(e);
       });
           
          astuceBtn.addActionListener(e -> {
           Session.setSingleAdId(id_jeu);
           new AstuceForm(res).show();
       });
          
    }
    
    public void participerAction(Evenement e) {
        ConnectionRequest req = new ConnectionRequest();
        req.setUrl("http://localhost/mobile/mobile1/insert.php?idevenement=" +e.getId() + "&idmembre=" + "khaoula oueslati");

        req.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);

                if (s.equals("success")) {
                    updateEvent(e);
        // nbparticipant.setText("Participation :" + e.getNbpartcipant() + "/" + e.getNbpartcipantMax());
                    Dialog.show("Confirmation", "ajout ok", "Ok", null);

                }

            }
        });
        NetworkManager.getInstance().addToQueue(req);

    }
   public void updateEvent(Evenement e) {
        e.setNbpartcipant(e.getNbpartcipant() + 1);
        ConnectionRequest req2 = new ConnectionRequest();
        req2.setUrl("http://localhost/mobile/mobile1/updateevenement.php?idevenement=" + e.getId() + "&nb=" + e.getNbpartcipant());
        NetworkManager.getInstance().addToQueue(req2);

    }
    
    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }

}