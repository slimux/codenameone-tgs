/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mycompany.Forms;

import com.mycompany.Entities.Session;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import org.json.JSONArray;
import org.json.JSONObject;
import com.codename1.components.MultiButton;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.table.TableLayout;
import com.mycompany.myapp.PIDEV;

public class ClassifiedForm extends BaseForm {
     Container container;
       private Resources theme111;
  public void side() {
        
        Image m = PIDEV.theme.getImage("menu.png");
        Command profile = new Command("My Profile") {
            @Override
            public void actionPerformed(ActionEvent evt) {
                PIDEV.ProfileForm = new ProfileForm(PIDEV.theme);
                ((ProfileForm) PIDEV.ProfileForm).execute();
            }
        };
        getToolbar().addCommandToSideMenu(profile);
        Command c = new Command("FAVORITES");
        Label l = new Label("FAVORITES") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l.getStyle().setFgColor(255);
        l.setUIID("Separator");
        c.putClientProperty("SideComponent", l);
        this.addCommand(c);
        this.addCommand(new Command("Store", PIDEV.theme.getImage("cart.png")) {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new MainWindowForm(theme111).show();
             
            }

        });      this.addCommand(new Command("Games") {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new ClassifiedForm(theme111).show();
            //PIDEV.mainForm.show();
            }

        });
           this.addCommand(new Command("Events") {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
                 new EventsForm(theme111).show();
           
                
              
                
               
             
            }

        });
        this.addCommand(new Command("Video") {
            public void actionPerformed(ActionEvent evt) {
              theme111 = UIManager.initFirstTheme("/theme1");
                 new VidiosForme(theme111).show();
            }

        });
        
        Command c1 = new Command("ACTIONS");
        Label l1 = new Label("ACTIONS") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l1.setUIID("Separator");
        c1.putClientProperty("SideComponent", l1);
        this.addCommand(c1);
        this.addCommand(new Command("Exit") {

            public void actionPerformed(ActionEvent evt) {
                Display.getInstance().exitApplication();
            }
        });

        this.addCommand(new Command("Logout") {

            public void actionPerformed(ActionEvent evt) {
//                if(FacebookConnect.getInstance().isFacebookSDKSupported()) {
//                    FacebookConnect.getInstance().logout();
//                } else {
//                    FaceBookAccess.getInstance().logOut();
//                    Login.login(main);
//                }

                PIDEV.loginForm = new LoginForm(PIDEV.theme);
                PIDEV.loginForm.show();
            }
        });

    }

    public ClassifiedForm(Resources res) {
       
        
//        super("Classified", BoxLayout.y());
//        Toolbar tb = new Toolbar(true);
//        setToolbar(tb);
//        getTitleArea().setUIID("Container");
//        setTitle("Games");
//        getContentPane().setScrollVisible(false);
//        
//       // super.addSideMenu(res);
//        
//       
//        
//        Tabs swipe = new Tabs();
//
//        Label spacer1 = new Label();
//        Label spacer2 = new Label();
//       // addTab(swipe, res.getImage("tmp.png"), spacer1, "", "", "L\'espace qui regroupe tous les Jeux!");
//                
//       // swipe.setUIID("Container");
//       // swipe.getContentPane().setUIID("Container");
//        swipe.hideTabs();
//        
//        ButtonGroup bg = new ButtonGroup();
//        int size = Display.getInstance().convertToPixels(1);
//        Image unselectedWalkthru = Image.createImage(size, size, 0);
//        Graphics g = unselectedWalkthru.getGraphics();
//        g.setColor(0xffffff);
//        g.setAlpha(100);
//        g.setAntiAliased(true);
//        g.fillArc(0, 0, size, size, 0, 360);
//        Image selectedWalkthru = Image.createImage(size, size, 0);
//        g = selectedWalkthru.getGraphics();
//        g.setColor(0xffffff);
//        g.setAntiAliased(true);
//        g.fillArc(0, 0, size, size, 0, 360);
//        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
//        FlowLayout flow = new FlowLayout(CENTER);
//        flow.setValign(BOTTOM);
//        Container radioContainer = new Container(flow);
//        for(int iter = 0 ; iter < rbs.length ; iter++) {
//            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
//            rbs[iter].setPressedIcon(selectedWalkthru);
//            rbs[iter].setUIID("Label");
//            radioContainer.add(rbs[iter]);
//        }
//                
//        rbs[0].setSelected(true);
//        swipe.addSelectionListener((i, ii) -> {
//            if(!rbs[ii].isSelected()) {
//                rbs[ii].setSelected(true);
//            }
//        });
//        
//        Component.setSameSize(radioContainer, spacer1, spacer2);
//        add(LayeredLayout.encloseIn(swipe, radioContainer));
//        
//      
//   
//        
//      
//        
//        
getStyle().setBgColor(0xe3e3e3);
side() ;
setTitle("Games");

//          MultiButton orderMb = new MultiButton();
//  
//        getStyle().setBgColor(0xe3e3e3);
//        
//          final TextField txt = new TextField();
//                txt.setHint("Rechercher");
//               getToolbar().setTitleComponent(txt);
//               
//                this.add(new button("Ok") {
//
//                    @Override
//                    public void actionPerformed(ActionEvent evt) {
//                    String  searchText=txt.getText();
//                      ConnectionRequest con= new ConnectionRequest(); 
//       con.setUrl("http://localhost/mobile/mobile2/connect.php");
//        con.setUrl("http://localhost/mobile/mobile2/searchGame.php?search=" + searchText);
//        
//        System.out.println("before connection!");
//        con.addResponseListener((NetworkEvent evt1) -> {
//            System.out.println("after  connection!");
//            String response = new String(con.getResponseData());
//            System.out.println("helloo");
//            
//            if(!response.equals("error")){
//                JSONArray a = new JSONArray(response);
//                removeAll();
//                for(Object o : a){
//                    
//                   System.out.println(o);
//                    JSONObject tmp = new JSONObject(o.toString());
//                   System.out.println("1");      
//                    Image adPic = URLImage.createToStorage((EncodedImage) res.getImage("jaquette-grand-theft-auto-v-playstation-4-ps4-cover-avant-g-1415122088.jpg"), 
//                                  tmp.getString("image"), 
//                                  ADS_URL + tmp.getString("image"));
//                  
//                     System.out.println("2");
//                    
//                         addButton(adPic, 
//                              tmp.getString("titre"), 
//                              tmp.getString("description"),
//                              false, 
//                              tmp.getInt("prix"), 
//                              32,
//                              tmp.getInt("id_jeu"),res);
//                         
//                    System.out.println("sayee");
//                }
//            }else
//                Dialog.show("Erreur","Probleme d\'affichage", "ok",null);
//            //
//        });
//        NetworkManager.getInstance().addToQueue(con);
//        
//                    
//                    }
//
//                });
//        
        
        
        
        
        
        
        
        
        
        
        ////////////////////////////////////////////////
        ConnectionRequest con= new ConnectionRequest(); 
        con.setUrl("http://localhost/mobile/mobile2/connect.php");
        con.setUrl("http://localhost/mobile/mobile2/getGame.php");
        System.out.println("before connection!");
        con.addResponseListener((NetworkEvent evt1) -> {
            System.out.println("after  connection!");
            String response = new String(con.getResponseData());
            
            if(!response.equals("error")){
                JSONArray a = new JSONArray(response);
                for(Object o : a){
                    JSONObject tmp = new JSONObject(o.toString());
                    Image adPic = URLImage.createToStorage((EncodedImage) res.getImage("jaquette-grand-theft-auto-v-playstation-4-ps4-cover-avant-g-1415122088.jpg"), 
                                  tmp.getString("image"), 
                                  ADS_URL + tmp.getString("image"));
                    
                     
                    
                    addButton(adPic, 
                              tmp.getString("titre"), 
                              tmp.getString("description"),
                              false, 
                              tmp.getInt("prix"), 
                              32,
                              tmp.getInt("id_jeu"),res);
                }
                
            }else
                Dialog.show("Erreur","Probleme d\'affichage", "ok",null);
            //
        });
        NetworkManager.getInstance().addToQueue(con);
       /*
        addButton(res.getImage("news-item-1.jpg"), "Morbi per tincidunt tellus sit of amet eros laoreet.", false, 26, 32);
        addButton(res.getImage("news-item-2.jpg"), "Fusce ornare cursus masspretium tortor integer placera.", true, 15, 21);
        addButton(res.getImage("news-item-3.jpg"), "Maecenas eu risus blanscelerisque massa non amcorpe.", false, 36, 15);
        addButton(res.getImage("news-item-4.jpg"), "Pellentesque non lorem diam. Proin at ex sollicia.", false, 11, 9);
        */
    }
    
    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();
    }
    
    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text) {
        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
        if(img.getHeight() < size) {
            img = img.scaledHeight(size);
        }
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
        }
        ScaleImageLabel image = new ScaleImageLabel(img);
        image.setUIID("Container");
        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
        
        Container page1 = 
            LayeredLayout.encloseIn(
                image,
                overlay,
                BorderLayout.south(
                    BoxLayout.encloseY(
                            new SpanLabel(text, "LargeWhiteText"),
                            FlowLayout.encloseIn(likes, comments),
                            spacer
                        )
                )
            );

        swipe.addTab("", page1);
    }
    Container ctn = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        
        Container ctn2 = BorderLayout.center(ctn);
   private void addButton(Image img, String title,String desc, boolean liked, int likeCount, int commentCount, int id_jeu, Resources res) {
    
int height = Display.getInstance().convertToPixels(19f);
     int width = Display.getInstance().convertToPixels(15f);
    Button image = new Button(img.fill(width, height));     image.setUIID("Label");
//      Container cnt = BorderLayout.west(image);
  //   cnt.setLeadComponent(image);
//       TextArea ta = new TextArea(title);
//       Label des = new Label(desc);
//       ta.setUIID("NewsTopLine");
//       ta.setEditable(false);
//
//       Label likes = new Label(likeCount + " Likes  ", "NewsBottomLine");
//       likes.setTextPosition(RIGHT);
//       if(!liked) {
//           FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
//       } else {
//           Style s = new Style(likes.getUnselectedStyle());
//           s.setFgColor(0xff2d55);
//           FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
//           likes.setIcon(heartImage);
//       }
//       Label comments = new Label(commentCount + " Comments", "NewsBottomLine");
//       FontImage.setMaterialIcon(likes, FontImage.MATERIAL_CHAT);
//       
//       
//       cnt.add(BorderLayout.CENTER, 
//               BoxLayout.encloseY(
//                       ta,
//                       des,
//                       BoxLayout.encloseX(likes, comments)
//               ));

 
               
  Label l=new Label(" ");
 MultiButton fourLinesIcon = new MultiButton(title);
Container tl = TableLayout.encloseIn(1,l, fourLinesIcon);
        
        ctn2.setLeadComponent(fourLinesIcon);
fourLinesIcon.setIcon(img.fill(width, height));
fourLinesIcon.setTextLine2(commentCount + " Comments");



 add(tl);
 fourLinesIcon.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent evt) {
        Session.setSingleAdId(id_jeu);
        new SingleAdForm(res).show();
    }
});
//            image.addActionListener(e -> {
//           Session.setSingleAdId(id_jeu);
//          new SingleAdForm(res).show();
//       });
      
   }
    
    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if(b.isSelected()) {
                updateArrowPosition(b, arrow);
            }
        });
    }
}
