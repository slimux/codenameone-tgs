/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.components.InfiniteProgress;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.mycompany.Controllers.UserController;
import com.mycompany.Entities.User1;
import com.mycompany.myapp.PIDEV;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/**
 *
 * @author haythem
 */
public class PaymentForm extends com.codename1.ui.Form {
public static final String ACCOUNT_SID = "AC723adac9da5bbd0d6e50cd38d757e13c";
public static final String AUTH_TOKEN = "af29632e92f982b2eab5619d78cb196a"; 
    InfiniteProgress progress;
    Dialog progressDialog;
    public void showProgress()
    {      
        if( progressDialog == null)
        {
            progress = new InfiniteProgress();
            progressDialog = progress.showInifiniteBlocking();
        }
    }
    
    public void hideProgress()
    {
        if(progressDialog != null)
        {
            progressDialog.dispose();
            progressDialog = null;
        }
    }
    
    public PaymentForm() {
        this(com.codename1.ui.util.Resources.getGlobalResources());
    }
    
    public PaymentForm(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
    }

//-- DON'T EDIT BELOW THIS LINE!!!


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.BorderLayout());
        setTitle("PaymentForm");
        setName("PaymentForm");
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!

    @Override
    public void show() {
        super.show(); 
    }
    
    public void execute(String strDetail, int amount){
        User1 user = new User1();
        setTitle("");
        getStyle().setBgColor(0xe3e3e3);
        TextFieldEx orderShipNameField = new TextFieldEx("", "Order Ship Name", 20, TextField.EMAILADDR) ;
        orderShipNameField.setText(User1.getCurrentUser().nom);
        TextFieldEx orderShipAddressField = new TextFieldEx("", "Order Ship Address", 20, TextField.EMAILADDR) ;
        orderShipAddressField.setText(User1.getCurrentUser().address);
        TextFieldEx orderCityField = new TextFieldEx("", "Order City", 20, TextField.EMAILADDR) ;
        orderCityField.setText(User1.getCurrentUser().city);
        TextFieldEx orderZipField = new TextFieldEx("", "Order Zip Code", 20, TextField.EMAILADDR) ;
        orderZipField.setText(User1.getCurrentUser().zip);
        TextFieldEx orderCountryField = new TextFieldEx("", "Order Country", 20, TextField.EMAILADDR) ;
        orderCountryField.setText(User1.getCurrentUser().country);
        TextFieldEx orderEmailField = new TextFieldEx("", "Order Email Address", 20, TextField.EMAILADDR) ;
        orderEmailField.setText(User1.getCurrentUser().email);
        
        Button orderButton = new Button("ORDER");
        orderButton.addActionListener(e -> {
            Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
              Message message = Message.creator(new PhoneNumber("+21693713248"),
                new PhoneNumber("++18177567558 "), 
                "Order is successfuly added").create();
                System.out.println(message.getSid());
            String orderShipName = orderShipNameField.getText();
            String orderShipAddress = orderShipAddressField.getText();
            String orderCity = orderCityField.getText();
            String orderZip = orderZipField.getText();
            String orderCountry = orderCountryField.getText();
            String orderEmail = orderEmailField.getText();

            String strParams = "{\"user_id\":" + User1.getCurrentUser().id + ", \"OrderAmount\":" + amount + ", \"OrderShipName\":\"" + orderShipName + "\", \"OrderShipAddress\":\"" + orderShipAddress + 
                    "\",\"OrderCity\":\"" + orderCity + "\", \"OrderZip\":\"" + orderZip + "\", \"OrderCountry\":\"" + orderCountry + "\", \"OrderEmail\":\"" + orderEmail + "\", " +
                    "\"details\":" + strDetail + "}";
            System.out.println(strParams);
            showProgress();
            new UserController().order(strParams);
            
            Dialog.show("Success", "Your Order has been successfuly added.", "OK", "Cancel");
        });
        
        Label spaceLabel;
        if(!Display.getInstance().isTablet() && Display.getInstance().getDeviceDensity() < Display.DENSITY_VERY_HIGH) {
            spaceLabel = new Label();
        } else {
            spaceLabel = new Label(" ");
        }
        
        Container by = BoxLayout.encloseY(
                spaceLabel,
                BorderLayout.center(orderShipNameField),
                BorderLayout.center(orderShipAddressField),
                BorderLayout.center(orderCityField),
                BorderLayout.center(orderZipField),
                BorderLayout.center(orderCountryField),
                BorderLayout.center(orderEmailField),
                orderButton
        );
        add(BorderLayout.CENTER, by);
        Command cmdBack = new Command("<back", FontImage.MATERIAL_PERSON_OUTLINE);
        getToolbar().addCommandToLeftBar(cmdBack);
        addCommandListener(e -> {
            if (e.getCommand() == cmdBack) {
                PIDEV.mainForm.showBack();
            }
        });
        by.setScrollableY(true);
        by.setScrollVisible(false);
        show();
    }
    
    
}
