/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.ui.Button;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.Controllers.UserController;
import com.mycompany.Entities.User1;

/**
 *
 * @author haythem
 */
public class LoginForm extends GeneralForm{
    
    Form LOGIN;
    TextField TF_Email;
    TextField TF_Pass;
    Button BtnSI;
    Button BtnSU;
    
    public LoginForm(Resources theme){ 
        super(theme);
        UIBuilder ui = new UIBuilder();
        LOGIN= ui.createContainer(theme, "LOGIN").getComponentForm();
        LOGIN.setToolbar(new Toolbar(false));
        TF_Email = (TextField) ui.findByName("TF_Email", LOGIN);
        TF_Pass  = (TextField) ui.findByName("TF_Pass", LOGIN);
        TF_Pass.setConstraint(com.codename1.ui.TextArea.PASSWORD);   
        
        BtnSI = (Button) ui.findByName("BtnSI", LOGIN);
        BtnSI.addActionListener((ActionListener) (ActionEvent evt) -> {           
            showProgress();
            User1 user= new User1();
            user.setEmail(TF_Email.getText());
            user.setPassword(TF_Pass.getText());
            new UserController().login(user, theme);
        });     
        
        BtnSU = (Button) ui.findByName("BtnSU", LOGIN);
        BtnSU.addActionListener((ActionListener) (ActionEvent evt) -> {
          SignUpForm signup = new SignUpForm(theme);
               signup.getSIGNUP().animateLayout(1000);
               signup.getSIGNUP().show();
        }); 
        
    }

    public Form getLOGIN() {
        return LOGIN;
    }

    public TextField getTF_Email() {
        return TF_Email;
    }

    public TextField getTF_Pass() {
        return TF_Pass;
    }

    public Button getBtnSI() {
        return BtnSI;
    }

    public Button getBtnSU() {
        return BtnSU;
    }

    public void setLOGIN(Form LOGIN) {
        this.LOGIN = LOGIN;
    }
    
    public void show(){     
      LOGIN.show(); 
    }
}
