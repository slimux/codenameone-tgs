/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.components.InfiniteProgress;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author haythem
 */
public class GeneralForm extends Form{
    InfiniteProgress progress;
    Dialog progressDialog;
    public void showProgress()
    {      
        if( progressDialog == null)
        {
            progress = new InfiniteProgress();
            progressDialog = progress.showInifiniteBlocking();
        }
    }
    
    public void hideProgress()
    {
        if(progressDialog != null)
        {
            progressDialog.dispose();
            progressDialog = null;
        }
    }
    
     public GeneralForm(Resources theme) {
         super(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER_ABSOLUTE));
     }
}
