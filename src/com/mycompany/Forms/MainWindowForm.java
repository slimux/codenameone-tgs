/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.components.InfiniteScrollAdapter;
import com.codename1.components.MultiButton;
import com.codename1.facebook.FaceBookAccess;
import com.codename1.facebook.User;
import com.codename1.io.NetworkEvent;
import com.codename1.l10n.L10NManager;
import com.codename1.l10n.ParseException;
import com.codename1.social.FacebookConnect;
import com.codename1.social.LoginCallback;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Tabs;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.list.GenericListCellRenderer;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Controllers.UserController;
import com.mycompany.Entities.Product;
import com.mycompany.myapp.PIDEV;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainWindowForm extends com.codename1.ui.Form {

    private Form main;
    private Resources theme111;
           
    public class Order {

        public int product_id = 0;
        public int quantity = 0;
        public double price = 0;
        public String name = "";
        public Label productLabel = null;
        public MultiButton orderMb = null;
        public Container orderContainer = null;
    }

    public static List<Order> orderList = new ArrayList<>();

    Container productContainer, shoppingContainer;
    private EncodedImage placeholder;

    public MainWindowForm() {
        this(com.codename1.ui.util.Resources.getGlobalResources());
    }

    public MainWindowForm(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
    }

//-- DON'T EDIT BELOW THIS LINE!!!
// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.BorderLayout());
        setScrollableX(false);
        setScrollableY(false);
        setTitle("MainWindowForm");
        setName("MainWindowForm");
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
//    @Override
    public void show() {
        this.removeAll();
        orderList.clear();
        placeholder = (EncodedImage) PIDEV.theme.getImage("games.png");
        side();
        getStyle().setBgColor(0xe3e3e3);

        Command cmdPayment = new Command("   ", PIDEV.theme.getImage("shopping_cart_white.png"));
        getToolbar().addCommandToRightBar(cmdPayment);
        addCommandListener(e -> {
            if (e.getCommand() == cmdPayment) {

                int nOrderAmount = 0;
                String orderText = "[";
                for (Order order : orderList) {
                    nOrderAmount += order.quantity;
                    if (order.quantity > 0) {
                        if (orderText.length() == 1) {
                            orderText += "{\"product_id\":" + order.product_id + ", \"detailName\":\"" + order.name + "\", \"detailPrice\":" + order.price + ", \"detailQuantity\":" + order.quantity + "}";
                        } else {
                            orderText += ",{\"product_id\":" + order.product_id + ", \"detailName\":\"" + order.name + "\", \"detailPrice\":" + order.price + ", \"detailQuantity\":" + order.quantity + "}";
                        }
                    }
                }
                orderText += "]";

                if (nOrderAmount == 0) {
                    Dialog.show("Warning", "There is no order.", "OK", "Cancel");
                    return;
                }

                PIDEV.paymentForm = new PaymentForm(PIDEV.theme);
                ((PaymentForm) PIDEV.paymentForm).execute(orderText, nOrderAmount);
            }
        });

        setTitle("");
        Tabs tab = new Tabs();
        productContainer = createProductContainer();
        shoppingContainer = createShoppingContainer();

        tab.addTab("Product", productContainer);
        tab.addTab("Shopping Cart", shoppingContainer);
        add(BorderLayout.CENTER, tab);

        InfiniteScrollAdapter.createInfiniteScroll(productContainer, new Runnable() {
            private int nCurOffset = 0;

            @Override
            public void run() {
                UserController userdao = new UserController();
                List<Product> results = userdao.fetchProducts(nCurOffset);
                Component[] listingsToAdd = new Component[results.size()];
                for (int iter = 0; iter < listingsToAdd.length; iter++) {

                    Order order = new Order();
                    order.product_id = results.get(iter).id_jeuxVideo;
                    order.price = results.get(iter).prix;
                    order.name = results.get(iter).titre;
                    order.quantity = 0;
                    orderList.add(order);
                    Container container = new Container(new BorderLayout());
                    MultiButton mb = new MultiButton();
                    String thumb_url = "http://localhost/mobile/product/image/" + results.get(iter).photo + ".jpg";
                    String guid = "product_image_" + results.get(iter).photo + "_jpg";
                    L10NManager lnm = L10NManager.getInstance();
                    String price_formatted = "$" + lnm.format(results.get(iter).prix, 2);
                    String name = results.get(iter).titre;
                    String description = results.get(iter).description;

                    
                    try {
                        mb.setIcon(URLImage.createToStorage(placeholder, guid, thumb_url, URLImage.RESIZE_SCALE_TO_FILL));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mb.setTextLine1(price_formatted);
                    mb.setTextLine2(name);
                    mb.setTextLine3(description);

                  
                    container.addComponent(BorderLayout.CENTER, mb);
                    Button minus = new Button();
                    minus.setText("−");
                    Container buttonContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                    buttonContainer.addComponent(minus);
                    Label label = new Label("0");
                    label.setAlignment(Label.CENTER);
                    label.getStyle().setFgColor(999999);
                    buttonContainer.addComponent(label);
                    Button plus = new Button();
                    plus.setText("＋");
                    buttonContainer.addComponent(plus);
                    Container buttonLayoutContainer = new Container(new BorderLayout());
                    buttonLayoutContainer.addComponent(BorderLayout.SOUTH, buttonContainer);
                    container.addComponent(BorderLayout.EAST, buttonLayoutContainer);
                    listingsToAdd[iter] = container;

                    order.productLabel = label;

                    Container orderContainer = new Container(new BorderLayout());
                    MultiButton orderMb = new MultiButton();
                    order.orderMb = orderMb;
                    String orderGuid = "order_product_image_" + results.get(iter).photo + "_jpg";

                    try {
                        orderMb.setIcon(URLImage.createToStorage(placeholder, orderGuid, thumb_url, URLImage.RESIZE_SCALE_TO_FILL));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    orderMb.setTextLine1(price_formatted);
                    orderMb.setTextLine2(name);
                    orderMb.setTextLine3(description);
                    orderMb.setTextLine4(String.valueOf(order.quantity));

                 
                    orderContainer.addComponent(BorderLayout.CENTER, orderMb);

                    Button close = new Button();
                    close.setText("✖");
                    close.addActionListener(evt -> {
                        order.quantity = 0;
                        removeOrderComponent(order.orderContainer);
                        label.setText(String.valueOf(order.quantity));
                        order.orderMb.setTextLine4(String.valueOf(order.quantity));
                    }
                    );
                    orderContainer.addComponent(BorderLayout.EAST, close);
                    order.orderContainer = orderContainer;

                    minus.addActionListener(evt -> {
                        if (order.quantity > 0) {
                            order.quantity--;
                        }
                        if (order.quantity == 0) {
                            removeOrderComponent(order.orderContainer);
                        }
                        label.setText(String.valueOf(order.quantity));
                        order.orderMb.setTextLine4(String.valueOf(order.quantity));
                    });
                    plus.addActionListener(evt -> {
                        order.quantity++;
                        if (order.quantity == 1) {
                            addOrderComponent(order.orderContainer);
                        }
                        label.setText(String.valueOf(order.quantity));
                        order.orderMb.setTextLine4(String.valueOf(order.quantity));
                    });
                }

                nCurOffset += results.size();
                InfiniteScrollAdapter.addMoreComponents(productContainer, listingsToAdd, results.size() > 0);
            }

        }, true);
        
        super.show();
        

    }
    
    
    
    
    
    
    

    public void side() {
        
        Image m = PIDEV.theme.getImage("menu.png");
        Command profile = new Command("My Profile") {
            @Override
            public void actionPerformed(ActionEvent evt) {
                PIDEV.ProfileForm = new ProfileForm(PIDEV.theme);
                ((ProfileForm) PIDEV.ProfileForm).execute();
            }
        };
        getToolbar().addCommandToSideMenu(profile);
        Command c = new Command("FAVORITES");
        Label l = new Label("FAVORITES") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l.getStyle().setFgColor(255);
        l.setUIID("Separator");
        c.putClientProperty("SideComponent", l);
        this.addCommand(c);
        this.addCommand(new Command("Store", PIDEV.theme.getImage("cart.png")) {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new MainWindowForm(theme111).show();
             
            }

        });      this.addCommand(new Command("Games") {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new ClassifiedForm(theme111).show();
            //PIDEV.mainForm.show();
            }

        });
           this.addCommand(new Command("Events") {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
                 new EventsForm(theme111).show();
           
                
              
                
               
             
            }

        });
        this.addCommand(new Command("Video") {
            public void actionPerformed(ActionEvent evt) {
              theme111 = UIManager.initFirstTheme("/theme1");
                 new VidiosForme(theme111).show();
            }

        });
        
        Command c1 = new Command("ACTIONS");
        Label l1 = new Label("ACTIONS") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l1.setUIID("Separator");
        c1.putClientProperty("SideComponent", l1);
        this.addCommand(c1);
        this.addCommand(new Command("Exit") {

            public void actionPerformed(ActionEvent evt) {
                Display.getInstance().exitApplication();
            }
        });

        this.addCommand(new Command("Logout") {

            public void actionPerformed(ActionEvent evt) {
//                if(FacebookConnect.getInstance().isFacebookSDKSupported()) {
//                    FacebookConnect.getInstance().logout();
//                } else {
//                    FaceBookAccess.getInstance().logOut();
//                    Login.login(main);
//                }

                PIDEV.loginForm = new LoginForm(PIDEV.theme);
                PIDEV.loginForm.show();
            }
        });

    }

    public void addOrderComponent(Container container) {
        shoppingContainer.addComponent(container);
    }

    public void removeOrderComponent(Container container) {
        shoppingContainer.removeComponent(container);
        shoppingContainer.revalidate();
    }

    public Container createProductContainer() {
        Container container = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        container.setScrollableY(true);
        return container;
    }

    public Container createShoppingContainer() {
        Container container = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        container.setScrollableY(true);
        return container;
    }
}
