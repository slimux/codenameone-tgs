/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;

import com.codename1.components.MultiButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import static com.codename1.ui.Component.LEFT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.PIDEV;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Khaoula
 */
public class VidiosForme extends BaseForm {
       private Resources theme111;    
 Container c=new Container(new BoxLayout(BoxLayout.Y_AXIS));

    public VidiosForme(Resources res) {
side();

add(c);
        setTitle("Videos");

        MultiButton orderMb = new MultiButton();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/mobile/mobile3/select.php");
      
        System.out.println("before connection!");
        con.addResponseListener((NetworkEvent evt1) -> {
            System.out.println("after  connection!");
            String response = new String(con.getResponseData());

            if (!response.equals("error")) {
                JSONArray a = new JSONArray(response);
                for (Object o : a) {
                    JSONObject tmp = new JSONObject(o.toString());
                    


                    addButton(tmp.getInt("id_Video"), 
                            tmp.getString("titre"),
                            tmp.getString("type_video"),  tmp.getString("date_ajout"),tmp.getString("code_video"),
                            
                           
                          tmp.getString("description"),
                           res);
                }

            } else {
                Dialog.show("Erreur", "Probleme d\'affichage", "ok", null);
            }
            //
        });
        NetworkManager.getInstance().addToQueue(con);
        /*
        addButton(res.getImage("news-item-1.jpg"), "Morbi per tincidunt tellus sit of amet eros laoreet.", false, 26, 32);
        addButton(res.getImage("news-item-2.jpg"), "Fusce ornare cursus masspretium tortor integer placera.", true, 15, 21);
        addButton(res.getImage("news-item-3.jpg"), "Maecenas eu risus blanscelerisque massa non amcorpe.", false, 36, 15);
        addButton(res.getImage("news-item-4.jpg"), "Pellentesque non lorem diam. Proin at ex sollicia.", false, 11, 9);
         */
    }

    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();
    }


    private void addButton(int id_video, String titre,  String type_video, String date_ajout, String code_video,  String description ,Resources res)
    {
            int height = Display.getInstance().convertToPixels(19f);
     int width = Display.getInstance().convertToPixels(15f);           
  Label l=new Label(" ");
 MultiButton fourLinesIcon = new MultiButton(titre);
Container tl = TableLayout.encloseIn(1,l, fourLinesIcon);
        Image img=res.getImage("File - Video.png");
        //ctn2.setLeadComponent(fourLinesIcon);
fourLinesIcon.setIcon(img.fill(width, height));
fourLinesIcon.setTextLine2(date_ajout);
getStyle().setBgColor(0xe3e3e3);


 add(tl);
     
      
        
      //  c.add(fourLinesIcon);
        fourLinesIcon.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent evt) {
        //Session.setSingleAdId(id_jeu);
      new VideoForm(titre,type_video, date_ajout,code_video,description,res).show();
    }
});

    }

    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if (b.isSelected()) {
                updateArrowPosition(b, arrow);
            }
        });
    }

   public void side() {
        
        Image m = PIDEV.theme.getImage("menu.png");
        Command profile = new Command("My Profile") {
            @Override
            public void actionPerformed(ActionEvent evt) {
                PIDEV.ProfileForm = new ProfileForm(PIDEV.theme);
                ((ProfileForm) PIDEV.ProfileForm).execute();
            }
        };
        getToolbar().addCommandToSideMenu(profile);
        Command c = new Command("FAVORITES");
        Label l = new Label("FAVORITES") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l.getStyle().setFgColor(255);
        l.setUIID("Separator");
        c.putClientProperty("SideComponent", l);
        this.addCommand(c);
        this.addCommand(new Command("Store", PIDEV.theme.getImage("cart.png")) {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new MainWindowForm(theme111).show();
             
            }

        });      this.addCommand(new Command("Games") {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
             new ClassifiedForm(theme111).show();
            //PIDEV.mainForm.show();
            }

        });
           this.addCommand(new Command("Events") {
            public void actionPerformed(ActionEvent evt) {
                theme111 = UIManager.initFirstTheme("/theme1");
                 new EventsForm(theme111).show();
           
                
              
                
               
             
            }

        });
        this.addCommand(new Command("Video") {
            public void actionPerformed(ActionEvent evt) {
              theme111 = UIManager.initFirstTheme("/theme1");
                 new VidiosForme(theme111).show();
            }

        });
        
        Command c1 = new Command("ACTIONS");
        Label l1 = new Label("ACTIONS") {

            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        l1.setUIID("Separator");
        c1.putClientProperty("SideComponent", l1);
        this.addCommand(c1);
        this.addCommand(new Command("Exit") {

            public void actionPerformed(ActionEvent evt) {
                Display.getInstance().exitApplication();
            }
        });

        this.addCommand(new Command("Logout") {

            public void actionPerformed(ActionEvent evt) {
//                if(FacebookConnect.getInstance().isFacebookSDKSupported()) {
//                    FacebookConnect.getInstance().logout();
//                } else {
//                    FaceBookAccess.getInstance().logOut();
//                    Login.login(main);
//                }

                PIDEV.loginForm = new LoginForm(PIDEV.theme);
                PIDEV.loginForm.show();
            }
        });

    }
}

