/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Forms;


import com.mycompany.Entities.Session;
import com.mycompany.Entities.Game;
import com.codename1.capture.Capture;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.CheckBox;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import static com.mycompany.Forms.CleanModern.extractFileName;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;


public class EditProfileForm extends BaseForm {

    private final String UPLOAD_DIR = "http://localhost/mobile/mobile2/";
     private String fileName;
    
    public EditProfileForm(Resources res) {
        super(" ", BoxLayout.y());
//        getTitleArea().setUIID("Container");
//        getContentPane().setScrollVisible(false);
        setTitle("edit game");
       getStyle().setBgColor(0xe3e3e3);
           MultipartRequest con= new MultipartRequest(); 
        con.setUrl("http://localhost/mobile/mobile2/connect.php");
        con.setUrl("http://localhost/mobile/mobile2/search.php?id_jeu="+Session.getSingleAdId());
        System.out.println("before connection!");
        con.addResponseListener((NetworkEvent evt2) -> {
               
            String response = new String(con.getResponseData());
            
           
                JSONArray a = new JSONArray(response);
                for(Object o : a){
                JSONObject tmp = new JSONObject(o.toString());
           Command back = new Command("") {

            @Override
            public void actionPerformed(ActionEvent evt) {
                new SingleAdForm(res).show();
                
            }

        };  
        
        Image img = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, UIManager.getInstance().getComponentStyle("TitleCommand"));
        back.setIcon(img);
       getToolbar().addCommandToLeftBar(back);
        getToolbar().setTitleCentered(true);
        setBackCommand(back);

           
       
        
     
        Button changePicBTN = new Button("Change picture");
        addStringValue("",changePicBTN);
        
         
       
           String[] characters = { "Action", "Aventure", "Sport", "Fps",
    "Reflexion" /* cropped */
};
Picker p = new Picker();
p.setStrings(characters);
p.setSelectedString(characters[0]);
p.addActionListener(e -> ToastBar.showMessage("You picked " + p.getSelectedString(), FontImage.MATERIAL_INFO));
    Game u = new Game();
        addStringValue("genre", p);
        
   
            TextField note_presse = new TextField(tmp.getString("note_presse"));
        note_presse.setUIID("TextFieldBlack");
        addStringValue("note presse", note_presse);
                  
             
             
      
               TextField note_joueur = new TextField(tmp.getString("note_joueur"));
        note_joueur.setUIID("TextFieldBlack");
        addStringValue("note_joueur", note_joueur);
         
               TextField description = new TextField(tmp.getString("description"));
        description.setUIID("TextFieldBlack");
        addStringValue("description", description);
        
                 TextField titre = new TextField(tmp.getString("titre"));
        titre.setUIID("TextFieldBlack");
        addStringValue("titre", titre); 
        
               TextField prix = new TextField(tmp.getString("prix"));
        prix.setUIID("TextFieldBlack");
        addStringValue("prix", prix);
              String[] character = { "Ps4", "Ps3", "Xbox one", "Xbox 360",
    "Pc" /* cropped */
};
Picker p1 = new Picker();
p1.setStrings(character);
p1.setSelectedString(character[0]);
p1.addActionListener(e -> ToastBar.showMessage("You picked " + p1.getSelectedString(), FontImage.MATERIAL_INFO));
addStringValue("console", p1);
                
                    TextField developpeur = new TextField(tmp.getString("developpeur"));
        developpeur.setUIID("TextFieldBlack");
        addStringValue("Developpeur", developpeur);
                    
                  Picker datePicker = new Picker();
       datePicker.setType(Display.PICKER_TYPE_DATE);
     
        addStringValue("date", datePicker);
        

        CheckBox cb1 = CheckBox.createToggle(res.getImage("on-off-off.png"));
        cb1.setUIID("Label");
        cb1.setPressedIcon(res.getImage("on-off-on.png"));
        CheckBox cb2 = CheckBox.createToggle(res.getImage("on-off-off.png"));
        cb2.setUIID("Label");
        cb2.setPressedIcon(res.getImage("on-off-on.png"));
        
        Button okBTN = new Button("Save Changes.");
        addStringValue("",okBTN);
        
         
           
         okBTN.addActionListener(e -> {
             
 new ClassifiedForm(res).show();
       });
        
        Button cancelBTN = new Button("Cancel and go back.");
        addStringValue("",cancelBTN);
        
        cancelBTN.addActionListener(e -> new SingleAdForm(res).show());
        
           changePicBTN.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {

                try {
                    MultipartRequest cr = new MultipartRequest();
                    String  filePath = Capture.capturePhoto(Display.getInstance().getDisplayWidth(), -1);
                    fileName= extractFileName(filePath) +"jpg";
                    cr.setUrl("http://localhost/mobile/mobile2/inserImage.php");
                    cr.setPost(true);
                    
                    String mime="image/jpeg";
                    cr.addData("file", filePath, mime); 
                    cr.setFilename("file", fileName);
                    
                    InfiniteProgress prog = new InfiniteProgress();
                    Dialog dlg = prog.showInifiniteBlocking();
                    cr.setDisposeOnCompletion(dlg);
                    NetworkManager.getInstance().addToQueueAndWait(cr);
                    
                } catch (IOException ex) {
                    
                }
            }
        });
        
        //Save the changes
        okBTN.addActionListener(e ->{
               
           
            u.setConsole(p1.getText());
            u.setTitre(titre.getText());
            u.setDeveloppeur(developpeur.getText());
            u.setGenre(p.getText());
            u.setNote_presse(Integer.parseInt(note_presse.getText()));
            u.setNote_joueur(Integer.parseInt(note_joueur.getText()));
            
            u.setDescription(description.getText());
                 u.setDate_sortie(datePicker.getDate());
             u.setPrix(Integer.parseInt(prix.getText()));
           
            u.setImage(Session.getTmpImage()+".png");
            
          
            con.setUrl("http://localhost/mobile/mobile2/connect.php");
            String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd");
        dateString = sdfr.format( u.getDate_sortie());
     
        con.setUrl("http://localhost/mobile/mobile2/update.php?id_jeu="+Session.getSingleAdId()+ "&titre=" + u.getTitre()+ "&genre=" 
                + u.getGenre()+ "&date_sortie=" + dateString + "&note_presse=" + u.getNote_presse()+ "&note_joueur=" 
                + u.getNote_joueur()+ "&description=" + u.getDescription()+ "&prix=" + u.getPrix()+ "&console=" 
                + u.getConsole()+ "&image=" + fileName+ "&developpeur=" + u.getDeveloppeur()+ "&rating=" 
                + u.getRating());
            {
                System.out.println("Unable to upload image!");
            }
               con.addResponseListener((NetworkEvent evt1) -> {
                
                if(!response.equals("error")){
                    
                    Dialog.show("","Sauvgarde effectuer avec succes", "ok",null);
                     
                      /* Twilio.setUsername(ACCOUNT_SID);
                                Twilio.setPassword(AUTH_TOKEN);
                                SmsSender.SendSMS("+21625261215",
                                          API_PHONE, 
                                          "Your code is : ");*/
                    Message m = new Message("Bonjour, nous vous informons qu'une nouvelle equipe vient d'etre cree. Veuillez consulter la liste. "+titre.getText());
Display.getInstance().sendMessage(new String[] {"slim.benyoussef@esprit.tn"}, titre.getText(), m);
                }else
                    Dialog.show("Erreur","Verifiez vos coordonnes", "ok",null);
                //
            });
                
            NetworkManager.getInstance().addToQueue(con);
        });
                }
        });
        NetworkManager.getInstance().addToQueue(con);
    }
                     
    
    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
    
      public static String extractFileName( String filePathName )
  {
    if ( filePathName == null )
      return null;

    int dotPos = filePathName.lastIndexOf( '.' );
    int slashPos = filePathName.lastIndexOf( '\\' );
    if ( slashPos == -1 )
      slashPos = filePathName.lastIndexOf( '/' );

    if ( dotPos > slashPos )
    {
      return filePathName.substring( slashPos > 0 ? slashPos + 1 : 0,
          dotPos );
    }

    return filePathName.substring( slashPos > 0 ? slashPos + 1 : 0 );
  }
}

