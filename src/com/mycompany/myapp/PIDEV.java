package com.mycompany.myapp;

import com.codename1.io.ConnectionRequest;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.Entities.Product;
import com.mycompany.Entities.User1;
import com.mycompany.Forms.AccueilForm;
import com.mycompany.Forms.GeneralForm;
import com.mycompany.Forms.LoginForm;
import com.mycompany.Forms.MainWindowForm;
import com.mycompany.Forms.SignUpForm;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * This file was generated by <a href="https://www.codenameone.com/">Codename
 * One</a> for the purpose of building native mobile applications using Java.
 */
public class PIDEV {

    private Form current;
    public static Resources theme,theme1;
    public static GeneralForm loginForm, signupForm, accueilForm;
    public static Form  mainForm, paymentForm, ProfileForm;
    public static User1 user = null;
    public static List<Product> productList = new ArrayList<>();

    public void init(Object context) {
        theme = UIManager.initFirstTheme("/theme");
//        theme1 = UIManager.initNamedTheme("/theme", "Theme1");

        // Enable Toolbar on all Forms by default
        Toolbar.setGlobalToolbar(true);

        // Pro only feature, uncomment if you have a pro subscription
        // Log.bindCrashProtection(true);
    }

    public void start() {
        if (current != null) {
            current.show();
            return;
        }
        if (loginForm == null) {            
            loginForm = new LoginForm(theme);
        }
        if (signupForm == null) {
            signupForm = new SignUpForm(theme);
        }
        if (accueilForm == null) {
            accueilForm = new AccueilForm(theme);
        }
        if (mainForm == null) {
            mainForm = new MainWindowForm(theme);
        }
        try {
            SplashScreen sps = new SplashScreen();
            sps.animateLayout(1000);
            sps.show();
        } catch (IOException ex) {

        }
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
            @Override
            public void run() {

                LoginForm login = new LoginForm(theme);
                login.getLOGIN().animateLayout(1000);
                login.getLOGIN().show();
            }
        }, 4000);
    }

    public void stop() {
        current = Display.getInstance().getCurrent();
        if (current instanceof Dialog) {
            ((Dialog) current).dispose();
            current = Display.getInstance().getCurrent();
        }
    }

    public void destroy() {
    }

}
