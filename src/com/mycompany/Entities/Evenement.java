/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Date;
import java.util.Map;

/**
 *
 * @author Khaoula
 */
public class Evenement {
     private int id, nbpartcipant, nbpartcipantMax;
   private String nom,image,Lieu,desc,date;
 float lon ,lat;

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    

    public Evenement(int id, int nbpartcipant, int nbpartcipantMax, String nom, String image, String Lieu, String date) {
        this.id = id;
        this.nbpartcipant = nbpartcipant;
        this.nbpartcipantMax = nbpartcipantMax;
        this.nom = nom;
        this.image = image;
        this.Lieu = Lieu;
        this.date = date;
    }

    public Evenement() {
    }

    @Override
    public String toString() {
        return   nom ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNbpartcipant() {
        return nbpartcipant;
    }

    public void setNbpartcipant(int nbpartcipant) {
        this.nbpartcipant = nbpartcipant;
    }

    public int getNbpartcipantMax() {
        return nbpartcipantMax;
    }

    public void setNbpartcipantMax(int nbpartcipantMax) {
        this.nbpartcipantMax = nbpartcipantMax;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLieu() {
        return Lieu;
    }

    public void setLieu(String Lieu) {
        this.Lieu = Lieu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
   

   
     //e.setId(Integer.parseInt(obj.get("id").toString()));
//                e.setNom(obj.get("nom").toString());
//                e.setImage(obj.get("image").toString());
//                e.setDate(obj.get("date").toString());
//                e.setDesc(obj.get("descr").toString());
//                e.setLieu(obj.get("lieu").toString());
//                e.setLon(Float.parseFloat(obj.get("leng").toString()));
//                e.setLat(Float.parseFloat(obj.get("lat").toString()));
//                e.setNbpartcipant(Integer.parseInt(obj.get("nbpartcipant").toString()));
//                e.setNbpartcipantMax(Integer.parseInt(obj.get("nbpartcipantMax").toString()));
//                listEtudiants.add(e);
//               

    public static Evenement parseProduct(Map<String,Object> data)
    {
        Evenement product = new Evenement();
        try
        {
            product.id = Integer.parseInt((String)data.get("id"));
        }catch(NumberFormatException e)
        {
            e.printStackTrace();
        }
          try
        {
            product.lon = Float.parseFloat((String)data.get("leng"));
        }catch(NumberFormatException e)
        {
            e.printStackTrace();
        } try
           {
            product.lat = Float.parseFloat((String)data.get("lat"));
        }catch(NumberFormatException e)
        {
            e.printStackTrace();
        }
        try
        {
            product.image = (String)data.get("image");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
               try
        {
            product.Lieu = (String)data.get("lieu");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
          try
        {
            product.nom = (String)data.get("nom");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            product.desc = (String)data.get("descr");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        
        try
        {
            product.nbpartcipant = Integer.parseInt((String)data.get("nbpartcipant"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
          try
        {
            product.nbpartcipantMax = Integer.parseInt((String)data.get("nbpartcipantMax"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
   
        try
        {
            product.date = (String)data.get("date");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
       
        
        return product;
    }

    public Evenement(int id, int nbpartcipant, int nbpartcipantMax, String nom, String image, String Lieu, String desc, String date, float lon, float lat) {
        this.id = id;
        this.nbpartcipant = nbpartcipant;
        this.nbpartcipantMax = nbpartcipantMax;
        this.nom = nom;
        this.image = image;
        this.Lieu = Lieu;
        this.desc = desc;
        this.date = date;
        this.lon = lon;
        this.lat = lat;
    }

   
   
  
    
}
