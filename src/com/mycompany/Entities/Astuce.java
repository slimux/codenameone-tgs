/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

/**
 *
 * @author slimu
 */

public class Astuce {
    private int idAstuce;
    private int jeu_id;
    private String title_astuce;
    private String description_astuce;
    private String image_astuce;

    public Astuce() {
    }

    public Astuce(int idAstuce, int jeu_id,String title_astuce, String description_astuce, String image_astuce) {
        this.idAstuce = idAstuce;
        this.jeu_id= jeu_id;
        this.title_astuce = title_astuce;
        this.description_astuce = description_astuce;
        this.image_astuce = image_astuce;
        
    }

    public Astuce(int jeu_id, String title_astuce,String description_astuce, String image_astuce) {
        
        this.jeu_id= jeu_id;
       this.title_astuce = title_astuce;
       
        this.description_astuce = description_astuce;
        this.image_astuce = image_astuce;
    }


    public int getIdAstuce() {
        return idAstuce;
    }

    public void setIdAstuce(int idAstuce) {
        this.idAstuce = idAstuce;
    }
    
    public int getJeuId() {
        return jeu_id;
    }

    public void setJeuId(int jeu_id) {
        this.jeu_id = jeu_id;
    }

    public String getTitle_astuce() {
        return title_astuce;
    }

    public void setTitle_astuce(String title_astuce) {
        this.title_astuce = title_astuce;
    }

    public String getDescription_astuce() {
        return description_astuce;
    }

    public void setDescription_astuce(String description_astuce) {
        this.description_astuce = description_astuce;
    }
    
        public String getImage_astuce() {
        return image_astuce;
    }

    public void setImage_astuce(String image_astuce) {
        this.image_astuce = image_astuce;
    }
    
    
}

