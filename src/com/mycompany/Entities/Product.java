/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Map;

/**
 *
 * @author silve
 */
public class Product {
    public int id_jeuxVideo = 0;
    public String titre = "";
    public String description;
    public int telechargements;
    public double prix;
    public String developpeur;
    public String categorie = "";
    public int stock = 0;
    public int approuve = 0;
    public String photo;
    
    
    public Product()
    {
        
    }
    
    public Product(int id_jeuxVideo, String titre, String description, int telechargements, double prix, String developpeur, String categorie, int stock, int approuve,String photo)
    {
        this.id_jeuxVideo = id_jeuxVideo;
        this.titre = titre;
        this.description = description;
        this.telechargements = telechargements;
        this.prix = prix;
        this.developpeur = developpeur;
        this.categorie = categorie;
        this.stock = stock;
        this.approuve = approuve;
        this.photo = photo;
    }

    public static Product parseProduct(Map<String,Object> data)
    {
        Product product = new Product();
        try
        {
            product.id_jeuxVideo = Integer.parseInt((String)data.get("id_jeuxVideo"));
        }catch(NumberFormatException e)
        {
            e.printStackTrace();
        }
        
        try
        {
            product.titre = (String)data.get("titre");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
            product.description = (String)data.get("description");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        
        try
        {
            product.telechargements = Integer.parseInt((String)data.get("telechargements"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
             product.prix = Double.parseDouble((String)data.get("prix"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
            product.developpeur = (String)data.get("developpeur");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
            product.categorie = (String)data.get("categorie");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
            product.stock = Integer.parseInt((String)data.get("stock"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
            product.approuve = Integer.parseInt((String)data.get("approuve"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
            product.photo = (String)data.get("photo");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        
        
        
        return product;
    }

    public int getId_jeuxVideo() {
        return id_jeuxVideo;
    }

    public void setId_jeuxVideo(int id_jeuxVideo) {
        this.id_jeuxVideo = id_jeuxVideo;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTelechargements() {
        return telechargements;
    }

    public void setTelechargements(int telechargements) {
        this.telechargements = telechargements;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDeveloppeur() {
        return developpeur;
    }

    public void setDeveloppeur(String developpeur) {
        this.developpeur = developpeur;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getApprouve() {
        return approuve;
    }

    public void setApprouve(int approuve) {
        this.approuve = approuve;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    
    
    
}
