/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import com.codename1.l10n.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Khaoula
 */
public class Commentaire {
  private String username,contenu,date;
    int id, idevent;


    public Commentaire(String username, String contenu, int idevent) {
        this.username = username;
        this.contenu = contenu;
        this.idevent = idevent;
          Long millis = System.currentTimeMillis();
        Date s = new Date(millis);
        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd");
        date = sdfr.format(s);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdevent() {
        return idevent;
    }

    public void setIdevent(int idevent) {
        this.idevent = idevent;
    }

    public Commentaire() {
    }
    
}
