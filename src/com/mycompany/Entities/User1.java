/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Map;


public class User1 {
    public int id,typeuser=0; 
    public double solde=0;
    public String username, email,password,nom, prenom, photo ,address,city,zip,country;
    protected static int currentId,currentTypeuser=0;
    protected static double currentSolde=0;
    protected static String currentUsername, currentEmail,currentPassword,currentNom, currentPrenom, currentPhoto , currentAddress, currentCity, currentZip, currentCountry;
    private static User1 currentUser=new User1();
    
    public static User1 getCurrentUser(){
        return currentUser;
    }
    public static void setCurrentuser(User1 currentuser) {
        User1.currentUser = currentuser;
    }
    
    public static void UserCurrent(int currentId, String currentUsername, String currentEmail,String currentPassword, String currentNom, String currentPrenom,String currentPhoto, int currentTypeuser,double currentSolde,String currentAddress,String currentCity,String currentZip,String currentCountry) {
        User1.currentId=currentId;
        User1.currentUsername=currentUsername;
        User1.currentEmail=currentEmail;
        User1.currentPassword=currentPassword;
        User1.currentNom=currentNom;
        User1.currentPrenom=currentPrenom;
        User1.currentPhoto=currentPhoto;
        User1.currentTypeuser=currentTypeuser;
        User1.currentSolde=currentSolde;
        User1.currentAddress=currentAddress;
        User1.currentCity=currentCity;
        User1.currentZip=currentZip;
        User1.currentCountry=currentCountry;
    }
    
     
    
    
    
    public User1(String username, String email,String password, String nom, String prenom,String photo, int typeuser,double solde,String address,String city,String zip,String country) {

        this.username = username;
        this.email = email;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.photo = photo;
        this.typeuser = typeuser;
        this.solde = solde;
        this.address = address;
        this.city = city;
        this.zip =zip;
        this.country = country;
    }

    public User1(int id, String username, String email,String password, String nom, String prenom,String photo, int typeuser,double solde,String address,String city,String zip,String country) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.address = address;
        this.city = city;
        this.zip =zip;
        this.country = country;
        this.id = id;
    }
    
    public User1(String username, String email,String password, String nom, String prenom,String address,String city,String zip,String country) {
        
        this.username = username;
        this.email = email;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.address = address;
        this.city = city;
        this.zip =zip;
        this.country = country;
    }

    public User1() {
    }
    
    public static User1 parseUser(Map<String,Object> data)
    {
        User1 user = new User1();
        try
        {
            user.id = Integer.parseInt((String)data.get("id"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
            user.username = (String)data.get("username");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            user.email = (String)data.get("email");
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            user.password = (String)data.get("password");
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            user.nom = (String)data.get("nom");
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            user.prenom = (String)data.get("prenom");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            user.photo = (String)data.get("photo");
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            user.typeuser = Integer.parseInt((String)data.get("typeuser"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        try
        {
            user.solde = Float.parseFloat((String)data.get("solde"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            user.address = (String)data.get("address");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            user.city = (String)data.get("city");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            user.zip = (String)data.get("zip");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            user.country = (String)data.get("country");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        
        
        return user;
    }

    public static int getCurrentId() {
        return currentId;
    }

    public static void setCurrentId(int currentId) {
        User1.currentId = currentId;
    }

    public static int getCurrentTypeuser() {
        return currentTypeuser;
    }

    public static void setCurrentTypeuser(int currentTypeuser) {
        User1.currentTypeuser = currentTypeuser;
    }

    public static double getCurrentSolde() {
        return currentSolde;
    }

    public static void setCurrentSolde(double currentSolde) {
        User1.currentSolde = currentSolde;
    }

    public static String getCurrentUsername() {
        return currentUsername;
    }

    public static void setCurrentUsername(String currentUsername) {
        User1.currentUsername = currentUsername;
    }

    public static String getCurrentEmail() {
        return currentEmail;
    }

    public static void setCurrentEmail(String currentEmail) {
        User1.currentEmail = currentEmail;
    }

    public static String getCurrentPassword() {
        return currentPassword;
    }

    public static void setCurrentPassword(String currentPassword) {
        User1.currentPassword = currentPassword;
    }

    public static String getCurrentNom() {
        return currentNom;
    }

    public static void setCurrentNom(String currentNom) {
        User1.currentNom = currentNom;
    }

    public static String getCurrentPrenom() {
        return currentPrenom;
    }

    public static void setCurrentPrenom(String currentPrenom) {
        User1.currentPrenom = currentPrenom;
    }

    public static String getCurrentPhoto() {
        return currentPhoto;
    }

    public static void setCurrentPhoto(String currentPhoto) {
        User1.currentPhoto = currentPhoto;
    }

    public static String getCurrentAddress() {
        return currentAddress;
    }

    public static void setCurrentAddress(String currentAddress) {
        User1.currentAddress = currentAddress;
    }

    public static String getCurrentCity() {
        return currentCity;
    }

    public static void setCurrentCity(String currentCity) {
        User1.currentCity = currentCity;
    }

    public static String getCurrentZip() {
        return currentZip;
    }

    public static void setCurrentZip(String currentZip) {
        User1.currentZip = currentZip;
    }

    public static String getCurrentCountry() {
        return currentCountry;
    }

    public static void setCurrentCountry(String currentCountry) {
        User1.currentCountry = currentCountry;
    }
    
    

    @Override
    public String toString() {
        return "User1{" + "id=" + id + ", typeuser=" + typeuser + ", solde=" + solde + ", username=" + username + ", email=" + email + ", password=" + password + ", nom=" + nom + ", prenom=" + prenom + ", photo=" + photo + ", address=" + address + ", city=" + city + ", zip=" + zip + ", country=" + country + '}';
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTypeuser() {
        return typeuser;
    }

    public void setTypeuser(int typeuser) {
        this.typeuser = typeuser;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

   

    
   
   
    
}
