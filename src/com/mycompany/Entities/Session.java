/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Random;


public class Session {
    private static Game   currentGame = null;
    private static String tmpGame     = "";
    private static String tmpImage    = "";
    private static int singleAdId;
    private static int event;
    

    public static String getTmpGame() {
        return tmpGame;
    }

    public static void setTmpGame(String tmpGame) {
        Session.tmpGame = tmpGame;
    }
    
    
    
    public static void setSession(Game u){
        if(currentGame == null)
            currentGame = u;
    }

    public static Game getSession(){
        return currentGame;
    }

    public static String getTmpImage() {
        return tmpImage;
    }

    public static void setTmpImage(String tmpImage) {
        Session.tmpImage = tmpImage;
    }

    public static int getSingleAdId() {
        return singleAdId;
    }

    public static void setSingleAdId(int singleAdId) {
        Session.singleAdId = singleAdId;
    }
    
    
    
    public static void destroySession(){
        if(currentGame != null)
            currentGame = null;
    }
    

   
}
